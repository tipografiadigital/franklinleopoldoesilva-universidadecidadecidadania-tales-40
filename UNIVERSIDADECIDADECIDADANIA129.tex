\chapter{Universidade, cidade, cidadania}

Seria interessante seguir o processo de separação entre a universidade e
a cidade, que deu origem ao que conhecemos hoje como cidade
universitária. Encontraríamos aí, provavelmente, um percurso pontuado de
equívocos conceituais e de dificuldades históricas, correspondentes aos
diversos aspectos que podem ser destacados nesse processo. A~universidade nasceu à sombra da Igreja, mas em pouco tempo
transformou"-se numa instituição em que a Igreja buscava apoio para a sua
edificação doutrinária e para a ampliação de sua influência cultural e
política. Se nos arriscamos ao excesso dizendo que a Igreja precisou da
universidade tanto quanto esta necessitou da Igreja, certamente não se
pode negar que a universidade configurou"-se como um poderoso instrumento
de que se serviu a Igreja para a consolidação de um perfil cultural da
civilização na maior parte do ocidente e durante muitos séculos. Mas a
história nos mostra que essa relação não foi sempre inteiramente marcada
pela docilidade do ``instrumento'', o que se observa desde o começo e
não apenas durante as crises de transformação no limiar da modernidade.
O~recrutamento clerical e a dependência institucional nunca lograram
anular inteiramente uma tensão, latente ou manifesta, que percebemos até
nos momentos de maior harmonia aparente, como nos casos da convivência
entre a Igreja e o aristotelismo, no período de formação da Escolástica,
e da censura dos escritos de São Tomás de Aquino.

A mesma tensão pode ser observada entre a universidade e o poder civil,
quando este, percebendo a importância da nova corporação, procurou de
várias maneiras toma"-la sob sua proteção e coloca"-la a seu serviço.
Disso, como se sabe, aproveitaram"-se os universitários nas ocasiões em
que precisaram contrapor"-se ao poder da Igreja. Estes primeiros ensaios
de incorporação da universidade pelo estado, tendo"-se mostrado desde o
início como um jogo entre dois poderes, talvez tenha contribuído para
moldar o perfil da universidade, na sua estrutura e organização, segundo
padrões diversos da hierarquia eclesiástica e civil. Em momentos como
estes, a tensão revela a \emph{finalidade} e o \emph{caráter específico}
da instituição, que se expressarão posteriormente nas reivindicações de
autonomia como condição essencial de sua própria existência. Diga"-se
desde logo que a autonomia, mesmo sendo \emph{real} (em muitos casos ela
é apenas formal), não elimina a tensão entre a universidade e o seu
contexto (religioso, estatal, social) porque o caráter
\emph{politicamente peculiar} do exercício da \emph{liberdade acadêmica}
leva inevitavelmente a conflitos, desde que a situação não seja
camuflada por estratégias de subserviência ou por controles indiretos da
atividade universitária.

A história da universidade nos indica assim essas figuras
institucionais, pelas quais a vemos à sombra da Igreja e à sombra do
Estado e, na escala da longa duração, a tensão, e mesmo a contradição,
ainda que abafada por longos períodos de inserção pacífica e completa.
Essas mesmas relações nos ajudam a compreender os vínculos entre a
universidade e a cidade como corpo político historicamente organizado.
Também nesse caso a ambigüidade entre autonomia e dependência
eventualmente eclode em conflitos. Pádua, por ex., o grande centro
aristotélico da Idade Média, poderia ser qualificada como uma cidade
universitária, no sentido em que a universidade era o que possuía de
mais importante e o que lhe dava prestígio. Ao mesmo tempo, era a
atividade econômica de Pádua que tornava possível o suporte econômico da
universidade, tanto em termos dos alunos que tinham condições
financeiras de mantê"-la, quanto em termos da comunidade e do governo da
cidade, que também participavam dessa manutenção, na medida em que ela
correspondia aos seus interesses. Se ampliarmos o horizonte histórico,
poderemos nos referir a Iena, Tübinguen e Louvain em sentido análogo,
abstraindo naturalmente, grandes diferenças: são cidades universitárias
porque se constituem --- política, histórica, social e culturalmente ---
em torno da universidade, e passam a se distinguir por isso. De alguma
maneira encontram uma forma de vida e de organização coletiva em que
logram combinar exigências nem sempre convergentes, se supomos as
diferenças existentes entre a organização política da cidade e a
organização política da universidade.

Pudemos falar até aqui de universidade à sombra da Igreja e à sombra do
Estado, bem como das tensões e contradições aí envolvidas, porque
falávamos do passado. Para nos referirmos à universidade atual temos que
falar de mercado, e não de universidade à sombra do mercado, mas sim de
universidade inserida no mercado. O~contexto social que hoje define a
figura histórico"-política da universidade é o mercado, de resto
onipresente em todos os aspectos das relações humanas. A~explicitação
dessa inserção varia conforme a tradição da universidade em cada país, e
assim também o ritmo em que o mercado incorpora a instituição
universitária, o que está ainda em função do ritmo em que absorve todas
as outras instituições, devorando o espaço público.\footnote{Para o caso europeu, e especialmente francês, cf. \versal{FREITAG}, Michel.
\emph{Le Naufrage de l'Université}, La Découverte/Nuit Blanche,
Paris/Québec, 1995, sobretudo a excelente análise das relações entre a
gestão tecnocrática do social e o advento da universidade
organizacional, ou a falência da universidade institucional, pgs. 9--72.}  É
evidente que nos países europeus, de longa tradição universitária, há
uma combinação de inércia e resistência que retarda o processo, mas não
o detém, até porque a impregnação da universidade na sociedade, nesses
países, significa muito mais a consolidação histórica de uma instituição
longeva do que propriamente a valorização da universidade por parte da
sociedade --- ao menos dos segmentos que lhe determinam os rumos. Nos
países periféricos, como a universidade nunca ganhou foro social
autêntico, como as mudanças são decididas a partir de interesses muito
mais imediatos e como há uma submissão praticamente inquestionada a
critérios extrínsecos (países centrais e organismos internacionais), a
velocidade de absorção da universidade pelo mercado acompanha o ritmo
acelerado de dissolução do espaço público, que tem como consequência a
transformação da educação universitária em bem de consumo, dando
seguimento ao que já havia ocorrido com a educação fundamental e média.

Se fosse o caso de acompanhar o processo pelo qual se passou de uma
universidade institucional para uma universidade organizacional ou de
mercado, uma das etapas a destacar nesse percurso seria o exame da
estratégia política de estímulo à rejeição social da universidade
pública, que em nosso país coincide, de forma orquestrada ou não, com a
mercantilização do ensino superior, a qual atingiu patamares fantásticos
nos governos \versal{FHC}, com a extraordinária proliferação de empresas de
ensino, que já se organizam para disputar o mercado da pós"-graduação,
uma vez que o de graduação encontra"-se relativamente saturado. A~rejeição a que nos referimos possui várias facetas interligadas que
atuam como causas: a despreocupação dos governos com o aumento das vagas
nas universidades públicas, criando assim a justificativa para a
proliferação das empresas privadas; a sonegação sistemática de recursos
para a educação superior, comprometendo dessa forma a atividade
universitária nas instituições públicas, e ensejando assim,em muitos
casos, a diminuição de nível e de prestígio; a remuneração insuficiente
dos servidores docentes e administrativos; a reforma da previdência,
anunciada com alarde de modo a provocar a aposentadoria de muitos
professores, sem a adequada reposição; a campanha intensa contra os
servidores públicos, entre os quais professores e funcionários das
universidades federais e estaduais, transferindo"-lhes a culpa que cabe
aos gestores de recursos públicos. Com essa estratégia, que é composta
de muitos outros elementos além dos mencionados, o governo \versal{FHC} tentou,
de forma totalmente explícita, completar o trabalho --- já realizado
pelos seus antecessores no que se refere aos níveis de educação
anteriores à universidade --- de desqualificação social do professor,
transformado em funcionário improdutivo, parasita da sociedade, no qual
qualquer investimento é injustificado. Essa cortina de fumaça, que se
mostrou eficiente junto à opinião pública, tem o objetivo mais amplo de
atender às exigências de corte drástico nas políticas sociais. O~governo
do \versal{PT} segue com fidelidade essa orientação, como mostra o recente
documento do Ministério da Fazenda, em que são amplamente proclamados a
ineficácia e o desperdício, \emph{inerentes} aos gastos sociais.

Compreende"-se assim o empenho dos governos em levar a sociedade a
rejeitar a universidade pública como ineficiente e supérflua, e a
aceitar como algo absolutamente normal que a educação seja tratada como
um produto entre outros, com possibilidade de opções entre as várias
ofertas do mercado. Pois bem, a cidade universitária é algo que está
perfeitamente de acordo com essa reação negativa que o poder estatal
pretende estimular na sociedade. Para compreende"-lo, basta atentar para
o contexto político que apressou a separação entre a universidade e a
cidade. O~exemplo da Universidade de São Paulo é bem eloqüente. Como se
sabe, o diferencial no projeto dos fundadores da \versal{USP} estava na Faculdade
de Filosofia, Ciências e Letras, núcleo formador e integrador,
constitutivo de um espírito de universidade que, pelo menos em
princípio, pretendia superar a figura da universidade como conglomerado
de escolas superiores.\footnote{Cf. a respeito \versal{FERNANDES}, Florestan. \emph{Universidade Brasileira:
Reforma ou Revolução?} Ed. Alga"-Omega, São Paulo, 1975, especialmente a
análise da ``escola superior"-problema'', pgs 51 ss.}  Talvez seja sintomático que essa
Faculdade não tenha tido, desde o seu nascimento e durante o período
mais significativo de sua existência, o seu ``lugar'', em pelo menos
dois sentidos. Primeiramente, as grandes escolas profissionais
``conglomeradas'', às quais interessava um simulacro de universidade
para que pudessem manter poder e privilégios, jamais permitiram que a
\versal{FFCL} ocupasse, nas instâncias de poder, o lugar que deveria corresponder
à escola"-núcleo da universidade. Com isso, o próprio núcleo gerador de
pensamento crítico foi impedido de atuar na condução dos destinos da
universidade. Em outras palavras, tentava"-se fazer do lugar central um
lugar abstrato, para que desse centro não irradiassem transformações que
pudessem influir na estrutura de poder da universidade. Por outro lado,
a \versal{FFCL} não teve um lugar para instalar"-se, tendo de peregrinar por
prédios emprestados até fixar"-se no edifício de número 294 da rua Maria
Antonia, tornando seu um lugar que não o era originalmente. À~Faculdade
de Filosofia só foi dado um ``lugar'' depois de duas ocorrências
\emph{terminais:} o desmembramento da Faculdade de Filosofia Ciências e
Letras, com a saída dos cursos de ciências que deram origem aos atuais
institutos, e sua transformação em Faculdade de Filosofia, Letras e
Ciências Humanas, por efeito da reforma universitária; e o incêndio do
prédio da rua Maria Antonia pelos grupos paramilitares sediados na
Universidade Mackenzie, com a conivência das autoridades de segurança
pública e pelo menos a omissão das autoridades universitárias.

Que os responsáveis pela segurança da cidade e pela integridade da
universidade não tenham feito nada para preservar o prédio da Faculdade
de Filosofia é fato significativamente revelador. Viram no episódio
ocasião para separar e segregar a única escola que, no contexto da \versal{USP},
possuía perfil autenticamente universitário, o que era expresso tanto no
seu ideário formação quanto na sua conduta crítica. Nesse sentido,
tentavam destruir um outro ``lugar'', que não era nem o prédio nem o
assento do Conselho Universitário: procuravam destruir o perfil de
liberdade acadêmica e política que a Faculdade tentava construir e
irradiar para o restante da universidade, e também --- aí está o mais
grave --- para a cidade. Com efeito, em 1968 a Faculdade de Filosofia e
a rua Maria Antonia transformaram"-se no polo de resistência ao regime
militar. Assim se explica o acordo tácito entre a autoridade civil, a
autoridade militar, o esquema paramilitar mackenzista e \emph{a própria
\versal{USP}}: mesmo sem poder e sem recursos, a Faculdade de Filosofia se havia
tornado a espinha dorsal da universidade enquanto crítica e resistência,
e essa espinha precisava ser quebrada.

Era preciso não deixar à Faculdade outra alternativa que não fosse a
retirada: nesse sentido é muito apropriado referir a desocupação da
Maria Antonia a figuras de retirantes e exilados, bem como à intenção,
talvez involuntariamente simbólica, por parte dos agressores, de que a
consumação pelo fogo levasse ao esquecimento, como se fazia outrora com
as casas dos condenados à morte, para que deles não restasse coisa
alguma, nenhuma referência ou lembrança. Era preciso matar a
memória.\footnote{Cf. a respeito \versal{CARDOSO}, Irene. \emph{Maria Antonia: a Interrogação sobre
um Lugar a partir da Dor}. \emph{in}  \versal{CARDOSO}, Irene. Para Uma Crítica do
Presente. Ed. 34, São Paulo, 2001. Veja"-se ainda, na mesma coletânea,
\emph{Maria Antonia: o Edifício de número 294}. Cf. também \versal{SIMÃO}, Azis,
\emph{Na Faculdade}. In \versal{SANTOS}, M.L., Maria Antonia: Uma Rua na
Contramão, Nobel, São Paulo, 1988.}  Daí a significação do recomeço: nova
configuração institucional, novo nome, outro \emph{lugar}. Um lugar sem
memória, neutro, longe, separado, segregado, mas ainda não um lugar
propriamente seu: algo provisório, longamente, se possível
indefinidamente provisório, para dificultar qualquer tentativa de
recomposição coletiva, política e acadêmica. A~precariedade proposital e
a própria forma dos ``barracões'' que abrigaram cursos da \versal{FFLCH} a partir
de 1969 não têm nada a ver com o acaso. Depois de destruir o
\emph{lugar} e, assim enfraquecer o símbolo do polo de agregação
contestadora, era preciso também prevenir a continuidade da irradiação
crítica e contestatária que emanava desse polo. Tentou"-se fazê"-lo por
via de uma ruptura nas condições em que antes ocorria a continuidade da
vivência entre a universidade e a cidade, entre a vida universitária e a
vida cidadã, nos seus diversos aspectos. Na rua Maria Antonia, essa
continuidade não existia apenas devido à localização urbana: esta era
apenas a causa ocasional de uma sociabilidade peculiar, construída na
incorporação criativa que constantemente se dava na relação de
reciprocidade entre a universidade e o contexto social. Daí a variedade
quase caótica das modulações políticas e culturais ali cotidianamente
gestadas.

É muito claro, portanto, que a ideia de \emph{cidade} universitária
comporta o significado de \emph{gueto}, e isso como se sabe, não apenas
entre nós. Que o projeto de cidade universitária seja anterior às
agitações políticas da Maria Antonia em nada desmente essa afirmação.
Pois não se trata apenas de uma segregação motivada por distúrbios
concretos (passeatas e manifestações), embora isso possa ter precipitado
os acontecimentos. Trata"-se de uma segregação da ideia crítica de
universidade através da anulação das condições de sua expressão pública.
Foi a impossibilidade de controlar essa expressão que gerou a ideia de
seu confinamento. Nesse sentido a separação corresponde ao propósito de
tolher o caráter público da expressão contestadora, transformando atores
e expectadores num círculo endógeno destinado a auto"-exaurir"-se. Se tal
propósito por si mesmo não explica totalmente o fenômeno de
despolitização da \versal{USP}, pelo menos deve ser visto como uma de suas causas
mais operantes. O~propósito do gueto é confinar toda e qualquer
expressão de diferença à homogeneidade, a ponto de tornar a manifestação
inexpressiva, o que equivale a reduzi"-la ao silêncio. Essa forma de
domesticar a contestação, enquadrando"-a em dispositivos que controlem e
disciplinem, é característica das democracias formais, em que há empenho
na minimalização dos conflitos e na marginalização de seus significados.

A conseqüência desse processo é a anulação da relação de cidadania entre
a universidade e a cidade, o que faz parte da estratégia de provocar a
rejeição social da instituição pública. A~universidade é vista como
excrescência e não como parte da cidade. Assim se impõe uma determinada
representação da exclusão recíproca entre instituição pública e espaço
público, como se a universidade pública não devesse ocupar o espaço da
cidade. O \emph{campus} afastado torna"-se então a representação
arquitetônica do isolamento e da fragmentação, que tendem a ser vistos
como \emph{naturais e necessários}. Ao mesmo tempo o imaginário popular
é encorajado a ver nesse distanciamento e nessa separação uma forma de
elitização da universidade e de discriminação da maioria da população, e
disso se aproveitam os arautos da privatização para contestarem o
sentido da universidade pública. De fato, o isolamento da universidade
pública a impede de manter uma relação ativa de cidadania com a
população. Mas isso acontece porque não interessa às verdadeiras elites
que uma tal relação se concretize: em geral, quando se fala da relação
entre universidade e sociedade, o que os governantes e a cúpula
universitária têm em mente é a relação com as empresas e a universidade
a serviço do capital, ou o público a serviço do privado, como prova
eloqüentemente o fenômeno da proliferação das fundações e a
impossibilidade de sequer disciplinar (quanto mais reverter) o processo.
Percebe"-se então que as \emph{cidades universitárias} inscrevem"-se num
universo processual mais amplo, cujo parâmetro principal é a dissolução
do espaço público e a desinstitucionalização da universidade.

A estratégia de rejeição utilizada dentro de uma planificação mais ampla
de desmoralização das instituições públicas e o desmantelamento
progressivo através de ações e omissões, impede que se possa pensar numa
``universidade para a cidadania'', isto é, a instituição ``como peça
importante no jogo sócio"-político de construção da cidadania, refletindo
sobre as possibilidades de um saber e de uma prática universitárias
voltadas a conduzir para o centro da vida pública brasileira a figura do
cidadão ativo (…)''.\footnote{\versal{MURICY}, Marília. \emph{Universidade para Cidadania}. \emph{in}  \versal{PINHEIRO}, L.H\,(org). Crises e Dilemas da Universidade Pública no Brasil, Editora da
Universidade Federal da Bahia, Salvador, 1995, pg.58.}  Para que a universidade
pudesse desempenhar esse papel, haveria que se promover uma ampla
discussão político"-institucional da inserção sócio"-histórica da
instituição e de sua função crítica na formação da consciência cidadã.
Para isso seria preciso superar tanto a visão de cidadania como
categoria jurídica quanto, de forma mais ampla, a visão exclusivamente
normativa da sociedade. Temos de convir que já nos é impossível
pretender sequer algo próximo a esse desideratum, porque a condição
primeira, hoje recusada com veemência em nome da eficácia
organizacional, seria que a própria universidade se dispusesse a
discutir as relações complexas entre o processo de conhecimento e o
poder político, visando a questão acima das injunções conjunturais e
partidárias e considerando principalmente o plano
ético.\footnote{\versal{MURICY}, Marília. Ob. Cit., p\,60.} 

Nesse sentido não deve surpreender que a relação entre universidade e
sociedade, que deveria ter no vínculo entre a universidade e a cidade
uma de suas expressões concretas, seja hoje tão somente objeto de
considerações formais e preceitualistas, isso para não mencionar a
extraordinária dose de hipocrisia presente nos discursos oficiais sobre
o assunto. Quando se fala dos exemplos de relação entre universidade e
sociedade nos países desenvolvidos, omite"-se, em geral, que essa
relação, nos Estados Unidos, ocorre entre as grandes universidades e o
complexo industrial"-militar, razão pela qual o desenvolvimento da
pesquisa é inseparável da alienação da autonomia aos interesses que
comandam o financiamento dos grandes projetos.\footnote{Ao comentar casos da espécie, Robert Paul Wolff cita duas instituições
importantes: ``Tenho em mente a decisão de um Reitor, ou de um conselho
de mantenedores que emprestam os recursos de uma universidade para atos
criminosos de agressão externa, como na Universidade Estadual de
Michigan, ou que permitem o isolamento de prédios e corredores para a
pesquisa secreta, como no Massachussets Institute of Technology (\versal{MIT}).''
(\versal{WOLFF}, R\,P\,\emph{O~Ideal da Universidade}. Editora \versal{UNESP}, São Paulo,
1993, p\,177).} 
Quando se chega a esse ponto, a impossibilidade da discussão ética
torna"-se elemento constitutivo da vida universitária. Como o mercado, em
seus vários níveis, é impulsionado de diferentes maneiras, de forma que
é preciso tanto despertar e exacerbar o desejo do pequeno consumidor
quanto provocar guerras dispendiosas, a universidade de mercado
insere"-se nesse processo, seja oferecendo"-se como objeto de consumo,
seja oferecendo aos manipuladores políticos do mercado subsídios, em
termos de produtos e ideias, para a manutenção e expansão do sistema.
Vivemos uma realidade tecnocientífica em que o progresso e o
aprimoramento dos \emph{meios}, isto é, a operacionalidade técnica,
contrasta com a mais completa indiferença quanto à \emph{finalidade}.
Daí a dissolução de qualquer norma e de qualquer valor, fenômeno
exaltado pelos entusiastas da chamada ``pós"-modernidade'', que parecem
não ver que essa diluição acontece como contrapartida de um incrível
fortalecimento do caráter sistêmico da realidade social, cujo controle
ultrapassa os mais radicais prognósticos de um Orwell ou de um
Huxley.\footnote{Cf. \versal{DUPAS}, Gilberto. \emph{Tensões Contemporâneas entre o Público e o
Privado}. Paz e Terra, São Paulo, 2003, especialmente pgs.11 ss.} 

O que prejudica uma autêntica relação de cidadania entre a universidade
e a cidade é o desaparecimento da identidade coletiva do indivíduo
cidadão. Numa relação orgânica entre o individual e o coletivo, é a
comunidade que fornece o lastro para que alguém possa vir a constituir
\emph{comunitariamente} a sua individualidade. Os laços comunitários
configuram a cidadania concreta, porque cidadão é aquele que está
vinculado à cidade numa relação política, isto é, intrínseca. Quando o
cidadão é apenas o \emph{habitante} da cidade, como ocorre na época
moderna, essa relação extrínseca não configura qualquer pertinência: a
cidadania torna"-se abstrata. Mais ainda, deparamo"-nos com uma
\emph{privatização do conceito de cidadania}, conseqüência do
individualismo exacerbado que caracteriza o nosso
tempo.\footnote{Cf. \versal{DUPAS}, Ob. Cit., p\,30. Talvez se possa associar a esse fenômeno de
privatização da cidadania a tendência a identificar a defesa da
cidadania com a proteção dos interesses privados, ao modo das
associações que se propõem a defender direitos do indivíduo enquanto
usuário ou consumidor.}  Trata"-se de um fenômeno social mais amplo,
que no entanto se reproduz nitidamente na universidade. A~competição,
muitas vezes feroz, seja no âmbito da ascensão na carreira, seja pela
obtenção de cargos ou pela produtividade faz com que o indivíduo
considere a universidade como um meio de competir e vencer pessoalmente,
ficando o empenho institucional relegado a plano secundário, quando não
desaparece. A~avaliação estimula o processo: as instâncias de avaliação
atuam como o \emph{grande olho} que a todos controla por meio de regras
gerais e impessoais, o que reduz o indivíduo a um elemento da cadeia de
produtividade.

A prevalência do individualismo se dá em detrimento da comunidade. Não
se pode dizer que a organização física das universidades em cidades
universitárias tenha feito surgir comunidades de professores,
funcionários e alunos. Pelos motivos mencionados acima, a comunidade
aparece no máximo, e assim mesmo em algumas ocasiões, como instrumento
para que o indivíduo se afirme como tal, ou como uma instância à qual
ele eventualmente recorre quando se sente prejudicado nos seus
interesses particulares. Dessa forma cria"-se o mito da responsabilidade
individual relacionada à produtividade e ao mérito: é dever de cada um
destacar"-se individualmente, o que é interpretado como a
responsabilidade que o vincula à instituição. Na verdade, a
responsabilidade institucional, a excelência da universidade do ponto de
vista do mérito coletivo e outras qualificações institucionais
constituem apenas pano de fundo para a valorização do êxito particular,
já que o destaque individual numa grande instituição é visto como mais
significativo do que numa outra de menor porte. Daí a valorização da
instituição no seu perfil \emph{oficial} (principalmente no plano dos
resultados quantitativos) e a desvalorização da \emph{comunidade} que
\emph{faz} a instituição, mas que \emph{ainda} não possui feição
suficientemente homogênea para se identificar completamente com o perfil
institucional \emph{oficial}. Claro está que a desvalorização da
instância do \emph{político} como vínculo comunitário, mesmo
enfraquecido, {} está muito presente nessa discriminação. O~grande
esforço que se desenvolve atualmente vai no sentido de abolir o caráter
político (ou o que resta dele) da instituição universitária; o objetivo
visado é claro: a abolição do caráter político é ao mesmo tempo a
dissolução do perfil institucional e público. Resta então a opção
organizacional, ou o loteamento privado do espaço público.

O que há nisso de surpreendente, se também a cidade (o país) segue esse
caminho, saudado duplamente pela maioria, por ser ao mesmo tempo o
melhor e o único possível? Não se associa atualmente a responsabilidade
governamental à manutenção e à ampliação do perímetro excludente da
cidadania? Não é o fosso que separa incluídos e excluídos a própria
marca registrada da eficiência tecnocrática no gerenciamento da economia
e da moralidade que deve vigorar na manutenção dos ``contratos''? Na
linha da modernização conservadora que ultimamente deu novo fôlego ao
neoliberalismo no país, a universidade certamente será, mais uma vez,
reformada. O~contexto político já nos pode antecipar algo dessas
modificações, pois ao que tudo indica, a universidade apenas terá que
completar seu processo de adaptação, que até aqui tem sido um pouco
retardado pelos estertores de um espírito crítico agonizante. A~imposição do pensamento único não deve deixar muito espaço para a
explicitação política de conflitos. A~conciliação se dará pelas formas
falsas da democracia e ninguém há de reclamar de uma eficácia que
provenha da acomodação ``sistêmica'' dos interesses. Uma hábil campanha
publicitária e uma boa exibição midiática poderão mostrar que o caminho
para que a universidade pública saia do seu isolamento ``elitista'' já
está aí, há muito tempo, à vista de todos: o mercado dos bens
educacionais, tão estimulado pelos governos. Há que se vencer a
renitência em relação à evolução e ao progresso e correr atrás do ensino
superior privado, para descontar o atraso na adoção do novo paradigma e
superar a ameaça de obsolescência. Os gestores da \versal{USP} já estão nesse
caminho há algum tempo, sendo que os procedimentos e os resultados
aparentemente têm sido bem aceitos, pois não há qualquer iniciativa de
discussão de critérios ou de outro tópico que envolva as significativas
mudanças no perfil institucional que vão ocorrendo como se fossem
simples ajustes administrativos. Se essa é a situação da maior
universidade do país, não seria fundada qualquer expectativa de que as
coisas pudessem seguir rumo diverso no sistema universitário brasileiro
como um todo.

Da Igreja ao Estado, do Estado ao Mercado: a universidade parece ter
completado o ciclo da sua existência --- e o bom"-senso indica que não há
mais utopias a projetar no futuro, nem mesmo esperanças de transformação
que possam ser ``responsavelmente'' alimentadas. No passado, muitos
princípios governaram, de fato e de direito, as relações entre
universidade e história, universidade e política, universidade e
sociedade, universidade e liberdade, universidade e cidadania, etc:
todos ruíram e trata"-se de abandonar rapidamente os destroços, esquecer
que um dia lá se abrigaram sonhos e projetos. O~presente e o futuro
pertencem aos que aderiram ao princípio de realidade.

Franklin Leopoldo e Silva

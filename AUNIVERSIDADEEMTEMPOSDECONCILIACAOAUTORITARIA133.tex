\chapter{A universidade em tempos de conciliação autoritária}

Ao cabo de 70 anos de trajetória universitária, o que devemos esperar de
uma comparação entre os objetivos iniciais da Universidade de São Paulo,
proclamados na fundação, e o que ela efetivamente se tornou, não é algo
da ordem de um \emph{balanço}, ou seja, a resultante de uma relação
entre o \emph{previsto} e o \emph{realizado}, mas a compreensão crítica
de um desenvolvimento histórico desde o início marcado por
contradições.\footnote{Essas contradições estão exaustivamente examinadas no estudo clássico de
\versal{CARDOSO}, Irene, \emph{A~Universidade da Comunhão Paulista} (O projeto de
criação da Universidade de São Paulo), Cortez Editora, São Paulo, 1982.}  O propósito desse texto é muito modesto:
pretendemos indicar alguns aspectos históricos e estruturais de uma
tensão constitutiva considerada tanto no interior da Universidade quanto
na relação entre a Instituição e o seu contexto social e político, e
apontar para a coincidência histórica entre o processo de recalque das
contradições e o movimento de dissolução da Universidade como
instituição política atuante no espaço público.

A que veio a \versal{USP}? Em grande parte, para reagir ao ``padrão brasileiro de
escola superior'' descrito por Florestan Fernandes. ``A escola superior
brasileira constituiu"-se como uma escola de \emph{elites} culturais,
ralas, e que apenas podiam (ou sentiam necessidade social de) explorar o
ensino superior em direções muito limitadas. Como a massa de
conhecimentos procedia do exterior e a sociedade só valorizava a
formação de profissionais liberais, a escola superior tornou"-se uma
escola de \emph{elites} de ensino magistral e unifuncional: cabia"-lhe
ser uma escola de transmissão dogmática de conhecimentos nas áreas do
saber técnico"-profissional, valorizadas social, econômica e
culturalmente pelos extratos dominantes de uma sociedade de castas e
estamental.''\footnote{\versal{FERNANDES}, Florestan. \emph{Universidade Brasileira: Reforma ou
Revolução?}, Editora Alfa"-Omega, São Paulo, 1975, pgs. 51--52.}  Desde o império até o início dos anos 30
prevaleceu o que se pode chamar de \emph{bacharelismo}: o diploma
superior como marca característica das elites e como instrumento formal
de reprodução da estrutura de poder. A única função do saber era a de
aparecer como símbolo do poder. Nesse sentido, o conhecimento não era
efetivamente valorizado em si mesmo, sendo sua aquisição apenas um
requisito para a confirmação de uma espécie de direito natural ao poder
nos indivíduos da classe dominante. Como o exercício do poder dependia
da origem estamental e não de méritos intelectuais, a escola superior
não podia ser mais do que um ritual de passagem a que deviam se submeter
os herdeiros das elites. A~esse desprezo pela verdadeira formação
intelectual correspondia o papel político"-instrumental da preparação
profissional: as ``grandes escolas'' não precisavam se preocupar com uma
atividade educacional criadora, expansiva ou aprofundada, pois o diploma
do profissional liberal era menos um atestado de sua capacidade do que
um passaporte para usufruir a parcela de poder que lhe cabia e os
benefícios daí decorrentes. Toda a educação superior se pautava assim
pela orientação ritualística e o interesse intelectual, quando surgia,
somente podia ser atendido pelas oportunidades de vida inteligente
ocasionalmente encontradas fora da escola. Com isso se mantinha a
correspondência entre uma educação paralisante em relação a qualquer
iniciativa crítica e uma sociedade governada por padrões oligárquicos de
hierarquia. Daí o dogmatismo e a unifuncionalidade mencionados por
Florestan Fernandes: ambos estavam a serviço de um ideário
ultraconservador.

Ora, o excesso de conservadorismo limita as possibilidades de progresso.
Foi este diagnóstico, efetuado principalmente por liberais ilustrados
paulistas, que colocou em xeque o perfil oligárquico da estrutura de
poder e detectou a ausência de um projeto educacional sintonizado com o
republicanismo e a modernidade, capaz de introduzir o país no ritmo do
capitalismo industrial e faze"-lo participar efetivamente do progresso
político e econômico. Para tanto seria preciso que o Brasil alcançasse,
ainda que com cerca de dois séculos de atraso, o estágio do
esclarecimento. Ou seja, seria preciso formar uma elite intelectual
capaz de formular projetos positivamente racionais de adaptação do País
à modernidade, indivíduos devidamente instruídos e instrumentados para
transfigurar os interesses de classe em interesse geral. A~complexidade
dos problemas estava a exigir da educação superior bem mais do que a
função de ornamento social do oligarca; este teria que ceder lugar a um
novo mandarim, pedagogicamente talhado para a função de coordenar a
estabilidade de uma sociedade que já não podia ocultar seus conflitos;
alguém que não apenas os sufocasse por via de suas prerrogativas de
classe, mas que os administrasse por via do uso político"-instrumental da
razão.

Dessa maneira se teria contraposto à velha escola superior a nova idéia
de universidade, a qual, no entanto, não se sobrepôs à realidade
consolidada, porque a ``idéia de universidade foi, de fato, adulterada.
O~que se chamou de `universidade' não tinha substância própria, nem ao
nível estrutural"-funcional, nem ao nível histórico. Era uma mera
conglomeração de escolas superiores e um recurso para preservá"-las,
fortalece"-las e difundi"-las, com suas magras virtudes e com seus
incontáveis defeitos.''\footnote{\versal{FERNANDES}, Florestan. Ob. Cit., p\,56.}  Como que para mostrar, se
preciso fosse, que a transformações em educação não se podem completar
sem alterações significativas na sociedade, o embate entre a concepção
avançada de universidade e o perfil conservador da escola superior
acabou por resultar numa subordinação do moderno ao arcaico, que nem
mesmo produziu um híbrido, já que as escolas superiores souberam
manter"-se francamente dominantes e fazer da mudança um meio de
continuarem tais e quais, em termos de hegemonia, prestígio e,
principalmente, poder. Três tentativas ilustram o que teria sido esse
fracasso de uma nova idéia de universidade.

A Universidade do Distrito Federal, fundada em 1935 e incorporada à
Universidade do Brasil em 1939, teria sido o caso mais rápido de
liquidação da nova idéia. No discurso de posse como reitor, Anísio
Teixeira deixa muito claro o propósito educacional da nova instituição,
sem poupar críticas acerbas ao sistema vigente: ``Esse país é o país dos
diplomas universitários honoríficos, é o país que deu às suas escolas
uma organização tão fechada e tão limitada, que substituiu a cultura por
duas ou três profissões práticas, é o país em que a educação, por isso
mesmo, se transformou em título para ganhar um
emprego.''\footnote{\versal{TEIXEIRA}, Anísio. A~Função das Universidades. \emph{in}  \versal{TEIXEIRA}, Anísio.
\emph{A~Universidade de Ontem e de Hoje}, Ed. \versal{UERJ}, Rio de Janeiro,
1998, pgs. 91--2.}  Ao reivindicar uma relação viva entre
universidade e cultura, o objetivo político de Anísio Teixeira é a
postulação da autonomia como condição do trabalho universitário
autêntico. Contrapunha"-se assim a uma dupla submissão: em primeiro lugar
a das escolas superiores à valoração social das profissões, mormente
aquelas prestigiadas à época e que correspondiam aos cursos das grandes
escolas de direito, de medicina e de engenharia. Nesse sentido a crítica
antecipa algo das considerações de Florestan Fernandes acerca do serviço
que as escolas profissionais superiores prestavam às classes dominantes.
Em segundo lugar, combatia também a idéia de que o ensino superior teria
única e exclusivamente finalidades tão somente utilitárias, o que ele
chamava de \emph{praticismo}, e que no limite se reduzia a uma
especialização técnica acrescida à alfabetização e ao lustro superficial
do pseudo"-letrado. Opunha"-se assim a dois aspectos do mesmo
reducionismo, por trás do qual via, bem em consonância com os tempos, a
estratégia de tolher a liberdade de ensino e pesquisa. A~heteronomia
intrínseca da escola profissional e o dirigismo sócio"-cultural do ensino
superior eram finalmente reveladores de um esforço para manter a
educação superior atrelada aos interesses das classes dominantes. A~definição dada por Anísio Teixeira da Universidade do Distrito Federal
não deixa dúvidas quanto à fidelidade à ``nova idéia''.

``É uma universidade cujas escolas visam o preparo do quadro intelectual
do país, que até hoje se tem formado ao sabor do mais abandonado e do
mais precário autodidatismo. Uma escola de educação, uma escola de
ciência, uma escola de filosofia e letras, uma escola de economia e
direito e um instituto de artes, com objetivos desinteressados de
cultura não podem ser demais no país, como não podem ser demais na
metrópole desse país.''\footnote{\versal{TEIXEIRA}, Anísio. Ob. Cit., p\,92.} 

O que ressalta ``na nova idéia'' é o \emph{desinteresse} que deve
caracterizar os \emph{objetivos} atinentes á cultura. Ou seja, a
universidade nasce com um compromisso tanto mais forte quanto mais
desvinculado dos \emph{interesses} imediatos do profissionalismo e do
bacharelismo. Esse compromisso com a cultura é, na verdade,
intrinsecamente político, embora distante da estrita instrumentalidade
que vincula a educação à hegemonia política das elites. ``A universidade
socializa a cultura socializando os meios de
adquiri"-la.''\footnote{\versal{TEIXEIRA}, Anísio. Ob. Cit., p\,99.}  Não se enganavam os opositores à nova
instituição: a vinculação crítica entre vida, cultura e liberdade é
potencialmente transformadora, e a circulação institucional de idéias
constitui um meio de democratização do conhecimento.

A outra tentativa --- e o outro fracasso --- é a Universidade de
Brasília, no entender de alguns estudiosos a experiência mais relevante
no que concerne à criação de uma universidade verdadeiramente nova. Mas
também nesse caso os pressupostos objetivos foram, de alguma maneira,
incontornáveis desde o princípio. ``Todavia, ela mesma [a UnB],
apesar de tudo, teve que compactuar com o passado. Conferiu uma posição
de relevo ás escolas profissionais; e desenvolveu uma ampla composição
estratégica, de conseqüências funcionais, com as representações, os
valores e certos expoentes humanos do antigo ensino superior
pré"-universitário.''\footnote{\versal{FERNANDES}, Florestan. Ob. Cit., p\,57.}  Ainda assim não houve tempo e
condições para que a nova experiência viesse a revelar seus resultados,
pois o golpe de 64 interrompeu o seu curso e as subseqüentes reformas
descaracterizaram totalmente o perfil funcional e acadêmico idealizado
pelos fundadores.

Finalmente, a Universidade de São Paulo, organizada em torno da
Faculdade de Filosofia, Ciências e Letras como seu núcleo irradiador,
suscitou de imediato a mais ampla resistência, sobretudo internamente, o
que fez com que à centralidade acadêmica da Faculdade de Filosofia
jamais correspondesse uma posição de poder que lhe permitisse
desenvolver completamente o seu projeto de formação. A~estratégia das
grandes escolas profissionais foi a de manter a universidade como um
conglomerado, com um mínimo de integração funcional, conservando o poder
por via de alianças ocasionais reiteradas de forma imediatista e
cortando sempre pela raiz qualquer tentativa de discussão de um projeto
político"-acadêmico. Isso se fez --- e se faz --- pela ocupação
sistemática dos espaços de discussão e de decisão, seja diretamente
pelos representantes das grandes escolas, seja, em épocas mais recentes,
pelos seus prepostos oriundos dos institutos surgidos após a reforma, ou
mesmo por integrantes das áreas de humanas, submissos ou solidários à
hegemonia vigente. Nesse sentido, o processo de isolamento da Faculdade
de Filosofia, Ciências e Letras foi relativamente rápido e eficaz o
bastante para neutraliza"-la como foco de inovação e de disseminação do
espírito crítico, a tal ponto que, com o passar do tempo, a identidade
da escola acabou tornando"-se um problema para ela mesma.

``Aos poucos, a própria Faculdade de Filosofia, Ciências e Letras,
largada a suas funções especializadas, acabou sendo parcialmente
condicionada pelos requisitos estruturais e dinâmicos do padrão
brasileiro de escola superior. Ela mesma uma universidade em miniatura,
converteu"-se numa típica `escola superior"-problema' atacada de
gigantismo. Apesar da colaboração maciça de professores estrangeiros de
altíssimo nível, apesar dos esforços incansáveis dos seus jovens
professores brasileiros (…) ela não conseguiu escapar nem à
tirania do meio"-ambiente nem à submissão desastrosa a um padrão de
integração estrutural arcaico.''\footnote{\versal{FERNANDES}, Florestan. Ob. Cit., pgs. 56--7.} 

A Universidade de São Paulo, principalmente por via da Faculdade de
Filosofia, Ciências e Letras, teve de lidar com dois passados.
Primeiramente a herança das grandes escolas profissionais, que foram
formalmente incorporadas à Universidade mas que nunca incorporaram a
``nova idéia'' universitária; pelo contrário, serviram"-se da nova
estrutura para realizar de modo mais proveitoso seus interesses
próprios. Nesse sentido, o isolamento da Faculdade de Filosofia,
Ciências e Letras deve"-se a ter sido ela a única escola verdadeiramente
gerada pela idéia de universidade. Em segundo lugar está a herança das
intenções fundadoras --- basicamente o propósito de preparar elites para
o exercício esclarecido do poder. A~\versal{USP} nasceu de um projeto educacional
forçosamente crítico em relação à tradição oligárquica, mas que era
também um projeto de substituição das antigas elites por algo próximo a
uma ideologia iluminista operante na escala da cultura local. Ora, o
desenvolvimento da criatura deveria contrariar os propósitos de seu
nascimento e da sua criação, pois não se pode ao mesmo tempo cultivar a
crítica e subsidiar intelectualmente um projeto de poder. Há uma
contradição inscrita no próprio projeto fundador, que se explicitaria
pouco depois. Os fundadores colocaram"-se contra a relatividade mesquinha
dos valores oligárquicos, imediatistas e utilitaristas, em nome de
valores ``absolutos'' e ``eternos'' da liberdade e da razão. Mas o
realismo político logo justificou o casamento da democracia com o
autoritarismo, a pretexto de defender a liberdade contra o totalitarismo
comunista.\footnote{Cf. \versal{CARDOSO}, Irene. Ob. Cit., em especial o capítulo 5.}  Assim, o espírito crítico teria que se
expressar numa cruzada em defesa da fé liberal, aí incluídas as medidas
de força necessárias à preservação da ``razão'' e da ``liberdade''. A~Faculdade de Filosofia, Ciências e Letras viu"-se então na contingência
de ter de constituir o seu presente ao mesmo tempo \emph{contra} o
passado oligárquico e contra as origens liberais. Essas duas linhas de
oposição ao passado nunca convergiram para um projeto de futuro --- e a
conseqüência disso é a indefinição do presente.

Por isso também se pode dizer que, em cada presente contraditoriamente
constituído, a \versal{USP} esteve dentro e fora de seu
tempo,\footnote{Cf. \versal{CARDOSO}, Irene. ``Texto de Apresentação da Universidade de São
Paulo.'' \emph{Catálogo da Universidade de São Paulo}. \versal{EDUSP}, São Paulo,
1996.}  refletindo"-o e recusando"-o, como uma
testemunha que quisesse ser simultaneamente a negação viva dos eventos a
que assistia e dos quais participava. Esse potencial negador intrínseco
ao espírito crítico exerceu"-se principalmente na Faculdade de Filosofia,
que assim fazia manifestar"-se a contradição, e certamente foi essa a
causa de ter sido ela repudiada pelo conjunto da Universidade que,
curvando"-se às injunções conservadoras, deixava transparecer uma
conciliação que na verdade representava apenas os vários lances do jogo
de interesses que ocorria nas instâncias de poder.

As dificuldades derivadas dessa constituição contraditória do presente,
à qual se acrescia naturalmente a obscuridade do futuro, incidiram de
forma contundente na identidade da Faculdade de Filosofia, Ciências e
Letras e repercutiram também no perfil da Universidade. A~Reforma
Universitária que separou os cursos básicos de ciências exatas e
naturais dos cursos de letras e ciências humanas, permitindo que os
primeiros se constituíssem como institutos autônomos, apenas representou
o desfecho de um processo que já vinha de algum tempo. Numa época em que
as ciências já se desenvolviam, nos países centrais, em organizações de
pesquisa fortemente especializadas e completamente fechadas sobre si
mesmas, financiadas pelo complexo industrial"-militar em franca expansão,
não seriam necessários olhos de águia para perceber que esse modelo,
fatalmente imposto, era incompatível com a permanência das ciências
empírico"-formais na Faculdade de Filosofia, Ciências e Letras. Os
parceiros científicos previram, mesmo numa época em que a evolução da
tecnociência não era tão acelerada, que nada atenderia menos aos seus
interesses do que compartilhar o destino da Faculdade. Para isso também
contribuiu, certamente, o que se pôde vislumbrar a partir das pressões
dos Estados Unidos e dos organismos internacionais no sentido de uma
reorganização educacional que favorecesse as novas condições do
capitalismo na América Latina, principalmente em termos de progresso
técnico"-industrial controlado. Já era suficientemente clara a condição
cada vez mais subalterna que as Humanidades passariam a ocupar no futuro
próximo.

É nesse sentido de adaptação às novas exigências decorrentes das
mudanças na dinâmica do gerenciamento econômico"-político que deve ser
pensada a questão da reforma universitária, tanto no que concerne às
modificações em nível federal quanto aquelas que ocorreram na
Universidade de São Paulo. Não é o caso de nos estendermos aqui sobre
isso.\footnote{Análises amplas e lúcidas sobre a questão encontram"-se em \versal{FERNANDES},
Florestan, \emph{Universidade Brasileira: Reforma ou Revolução}, ob.
Cit., especialmente os capítulos 3, 6, 7 e 8.}  Fiquemos apenas com a definição política
dada por Florestan Fernandes: ``reforma consentida'', porque isso nos
permite visão um pouco mais abrangente. Com efeito, não se trata apenas
de classificar de inócuas as alterações propostas e efetivadas, sob o
pretexto de que o regime autoritário não podia permitir que se fosse até
o fundo das questões, restando assim uma certa cumplicidade de fato
entre a ditadura e a universidade, com o irremediável comprometimento de
qualquer tentativa de reforma. Pelo contrário, a universidade queria a
reforma, e a via como algo necessário para dar continuidade ao
cumprimento de sua tarefa. Ainda mais, dentre as modificações sugeridas,
sobretudo no Relatório Ferri, várias vão ao encontro de aspirações
nascidas de reflexões e discussões internas à instituição, como é o caso
da extinção da cátedra. Assim, resultados de teor progressista puderam
ser obtidos num contexto político marcado por extremo autoritarismo. O~que explica essa contradição em última instância é o seu próprio
ocultamento. Os proponentes da reforma, no caso da \versal{USP}, não quiseram ou
não puderam valer"-se da oportunidade para reavaliar o impulso inicial da
Universidade para verificar que tipo de incidência os quase 30 anos de
dinâmica histórica tiveram no projeto inicial, de que maneira
aprofundaram as ambigüidades ou que elementos dessas contradições teriam
sido sufocados ou anulados. As pressões políticas internas e externas,
bem como a necessidade de atender necessidades imediatas, impediram que
a reforma fosse uma ocasião para transformações mais profundas, que
somente poderiam advir de uma reflexão efetiva e profundamente histórica
sobre a Universidade. Em vez disso o Relatório alinhava, a título de
considerações gerais e introdutórias, um arremedo de síntese teórica e
histórica de caráter vago e generalista, baseado num ecletismo
espiritualista ultrapassado e vazado em jargão filosofante inteiramente
vazio. É~claro que isso não se deve apenas às idiossincrasias dos
redatores ou ao desatamento da veia especulativa nos amantes do
transcendental. Trata"-se da tentativa de firmar a reforma universitária
em bases a"-históricas. Não devemos deixar que o caráter canhestro do
empreendimento oculte o horizonte ideológico, porque a falta de um
projeto histórico intrínseco à reforma deve"-se a essa concepção da
universidade como um fim em si mesma e completamente desvinculada do
ritmo das transformações sociais. A~história não pode gerar demandas à
universidade porque esta seria independente da história. Assim, pode"-se
``reformar'' a universidade, o que em princípio é atender a uma demanda
histórica, sem analisar historicamente a complexidade de sua inserção
\emph{autônoma} no cenário dos fatores \emph{determinantes}.

Mas é possível, por outro lado, num aparente esforço de inserção
histórica, fazer um uso instrumental da história para estabelecer
valores que justifiquem determinados rumos para a universidade, os quais
seriam garantidos por via da ocupação do poder pelos defensores de tais
critérios. Foi o que ocorreu na \versal{USP} no período de redemocratização do
país. Desfeito o amplo arco de oposição ao regime autoritário,
revelaram"-se as tendências nascidas da introjeção de pressões
modernizadoras exercidas pelo centro do capitalismo sobre a sua
periferia. Como a universidade poderia \emph{ajustar"-se} às novas
exigências de gerenciamento tecnológico e mercadológico do ensino e da
pesquisa? Era preciso que esse ajustamento proviesse do interior da
instituição, para criar a impressão de que a \emph{adaptação} teria sido
uma \emph{opção} livre e racional. Ora, tecnociência e mercado não
combinam com crítica política da universidade nem com posições éticas a
respeito da produção e disseminação do conhecimento. Seria preciso
portanto eliminar a dimensão ético"-política da vida universitária,
conferindo à instituição uma estrutura funcional capaz de absorver as
diretrizes tecnocráticas que se vão tornando hegemônicas em todas as
instâncias de organização da sociedade. Assim aparecem, como
\emph{exigências universitárias}, requisitos organizacionais de eficácia
produtiva, erigindo"-se o modelo da qualidade empresarial privada como o
único a ser seguido pela instituição pública. Para obter esse resultado
é preciso que haja um desmonte político da vida universitária, que a
comunidade intelectual passe a observar como única regra de convivência
o preceito liberal de competitividade máxima e que as estruturas de
poder se definam, ao mesmo tempo, por fortes mecanismos de controle e
por fracos liames de representatividade, numa realização quase perfeita
de democracia formal. A~hierarquia meritocrática estabelece então o jogo
das exclusões: ``Nos anos 80, eis que o relatório \versal{GERES} coloca em pauta
a oposição `universidade alinhada/universidade do conhecimento',
universidade politizada e comprometida com as forças populares'
contraposta ao `projeto modernizante, baseado em paradigmas do
desempenho acadêmico e científico.'\,''\footnote{\versal{CARDOSO}, Irene. A~Universidade e o Poder. \emph{in}  \versal{CARDOSO}, Irene. \emph{Para
uma Crítica do Presente}, Editora 34, São Paulo, 2001, p\,48.}  O citado
relatório está em perfeita consonância com manifestações de vários
docentes da \versal{USP}. Pode"-se na verdade falar de uma campanha organizada,
com a publicação de artigos na imprensa, aos quais faziam eco editoriais
dos grandes jornais, alertando para o perigo da ``politização'' da
universidade. O~contexto mostra com clareza que a ``defesa da
qualidade'', o repúdio à ``mediocridade'' e ao ``populismo'', a
exaltação da ``competência'' etc. inscreviam"-se já numa trajetória de
despolitização, que veio a se tornar um projeto explícito para toda a
universidade brasileira nos anos do governo \versal{FHC}.

O curioso é que essa modernização politicamente regressiva era apregoada
em nome da fidelidade da universidade às transformações históricas,
operando assim uma confusão, certamente deliberada, entre inserção
histórica e adaptação à conjuntura. Ademais, esse esforço de
despolitização acontecia logo depois da redemocratização, a qual,
esperava"-se, deveria ensejar uma ampla oportunidade de discussão
democrática na universidade, que pudesse chegar mesmo a constituir"-se
como uma refundação institucional. Tal não aconteceu porque a
implementação de um projeto tecnoburocrático de universidade
organizacional, em ritmo acelerado, ultrapassou definitivamente qualquer
possibilidade de reorganização política do espaço público acadêmico. A~dissolução do espaço público universitário faz medrar um autoritarismo
que nem sempre é reconhecido como tal porque não se contrapõe a uma
efetiva vontade política de democratização. A~conseqüência, no limite, é
aquela que vivemos hoje: considerar como inócua qualquer posição
\emph{oposicionista}, assimilando"-a a uma pura e simples transgressão
gratuita, como se a \emph{crítica} que, não obstante todas as
contradições de seu exercício, foi o princípio formador da \versal{USP}, não
passasse de uma doença infantil da qual nos curamos na maturidade, e na
qual jamais voltaremos a recair.

Essa estigmatização da tradição crítica explica não apenas a omissão
institucional na discussão das questões cruciais, ou a opção pelas
soluções técnicas como forma de contornar as contradições fundamentais,
como também a ausência de uma discussão \emph{radical} acerca da
universidade. A~crítica somente abalará princípios estabelecidos se ela
mesma estiver profundamente enraizada no seu próprio princípio, que é a
liberdade, garantia da firmeza do espírito crítico e do alcance do
exame. Nesse sentido é a perda de princípios radicais que impede a
crítica conseqüente dos princípios superficiais e oscilantes que pululam
no discurso tecnocrático dos gestores da modernização. Não somos mais
capazes de uma crítica radical da universidade porque perdemos de vista
o seu princípio, na unidade múltipla e convergente das dimensões
cultural, política e institucional. Essa incapacidade é do mesmo gênero
do comportamento reativo que manifestamos em relação ao tecnocratismo
econômico vigente: não chegamos a nos opor verdadeiramente à tecnocracia
economicista porque não agimos no sentido de deslocar o discurso
tecnocrático para o terreno da discussão política. Aceitamos o jogo da
competência dissimuladora e do pragmatismo propositivo quando evitamos
\emph{negar simplesmente} a transfiguração da unilateralidade do
discurso tecnocrático em universalidade da razão. Estamos além da perda
dos princípios; perdemos a capacidade de indagar sobre eles e de
busca"-los.

``O princípio fundamental é hoje, em meio a tecno"-superfícies sem
densidade histórica, já irrecuperável para o saber. Neste sentido o fim
da filosofia como pensamento do fundamento último (ou pelo menos o fim
das faculdades de filosofia como morada dessa reflexão) coincide com o
fim da universidade em seu sentido moderno.''\footnote{\versal{AVELAR}, Idelber. \emph{Alegorias da Derrota: a Ficção Pós"-ditatorial e o
Trabalho do Luto na América Latina}. Editora \versal{UFMG}, Belo Horizonte, 2003,
p\,95.}  Se
formularmos kantianamente a pergunta pela condição de possibilidade da
universidade hoje, talvez não possamos ir muito além das justificações
de fato, talvez tenhamos que abandonar a investigação do princípio. Há
uma grande obscuridade no plano das condições institucionais, culturais
e políticas da universidade, motivo pelo qual por vezes ela aparece como
um ente que sobreviveu a si mesmo e que não tem mais razão de existir.
Há alguma categoria ou algum quadro histórico a que possamos remeter a
universidade? Como situa"-la, por ex., diante da hegemonia do mercado? A
crítica dessa hegemonia, em princípio, se faz em nome de valores
universais que permitem julgar eventuais desequilíbrios históricos em
que a atividade mercantil aparece como o fundamento das relações
humanas. Para isso é preciso que a crítica se faça a partir de
instâncias ainda não inteiramente submetidas à mercantilização e em que
a \emph{reflexão} ainda tenha condições de enfrentar a
\emph{reificação}. Até algum tempo atrás, esse era o espaço da
universidade --- e na medida mesma em que ela podia discutir
internamente a contradição por que passava a própria educação:
\emph{formação} ou \emph{aquisição} de bens educacionais? Exercício do
espírito crítico ou treinamento de habilidades?

Acontece que na nossa época a hegemonia do mercado não aparece como
desequilíbrio histórico, mas sim como realização da história. Nesse
sentido já não se trata de hegemonia, mas de \emph{universalização}.
Ora, admitido o mercado como valor universal, no qual a educação se deve
inserir (vide a sua consagração como \emph{bem} e \emph{serviço} pela
\versal{OMC}), já não há mais espaço em que a crítica se possa exercer a partir
de outro valor. Daí qualquer contestação aparecer como algo doentio ou
exótico. Isso é bem claramente ilustrado pelo esforço que tem sido feito
desde a redemocratização para que a Faculdade de Filosofia se adapte ao
contexto universitário de uma organização voltada para o mercado. A~reciprocidade desse esforço, que é desenvolvido tanto a partir de
pressões externas quanto internas, contribui para mostrar que se trata
de um processo irreversível, dada a orientação histórico"-política de
desinstitucionalização da universidade. ``Se a racionalidade moderna se
constitui através de uma chamada à \emph{universalidade}, base
fundacional da universidade moderna, a persistência de espaços não
mercantilizados, não reificados, representava a alavanca possível de
todas as críticas, modernamente formuladas, dessa mesma racionalidade
(…) Num momento em que a mercantilização chega a um estágio
verdadeiramente universal, o próprio fundamento dessa universalidade se
torna impensável, pela ausência de um exterior de onde seu projeto possa
ser vislumbrado.''\footnote{\versal{AVELAR}, Idelber. Ob. Cit., p\,96.}  Em outras palavras, o
mercado tornou"-se princípio constitutivo e o seu alcance é tal que
ultrapassa em muito a dimensão das operações mercantis, impondo"-se como
paradigma formal de todas as relações sociais e mesmo humanas. Ao
incorporar o modelo, a universidade dissolve a sua própria
universalidade, que não é um princípio abstrato, mas a tarefa
continuadamente concreta de pensar a totalidade. É~nesse sentido que se
pode falar do triunfo da reificação sobre a reflexão --- vitória tanto
mais perversa quanto a absorção do paradigma vem travestida de uma nova
configuração acadêmica, mais ``moderna'' e mais adequada à dinâmica da
``sociedade atual''. A~dissolução da universalidade como princípio
constitutivo da universidade provoca a situação, aparentemente
paradoxal, de um vazio inteiramente preenchido e que portanto não é
sentido como tal, o que se explica pelo fato de que a dissolução da
universidade é vista como sua transformação. Mas como não se trata de um
processo autônomo, não se pode pensar numa redefinição institucional
interna; é antes uma reconfiguração heterônoma, guiada por fatores
extrínsecos que se resume numa desfiguração, processo terminal em que a
universidade vai parodiando a si mesma enquanto fortalece os mecanismos
de esquecimento do seu passado, de cumplicidade com a facticidade do
presente e de compromisso cego com o futuro. Como tudo se passa em nome
da profissionalização e da eficácia, com ênfase nos \emph{resultados},
perde qualquer sentido a crítica dos \emph{fundamentos}. Essa é uma
conseqüência não apenas do triunfo do modelo mercadológico"-produtivista,
mas sobretudo da valorização da própria noção de \emph{modelo} como
princípio de organização e gestão: a adequação a um modelo ocorre
simplesmente a partir da funcionalidade dos elementos, vista a partir de
critérios de eficiência organizacional, sem qualquer preocupação com
sentido ou fundamento, seja do ponto de vista histórico, seja do ponto
de vista conceitual. Não é preciso insistir no esvaziamento
político"-institucional implicado nessa trajetória.

Mas talvez seja conveniente mencionar algo acerca do esvaziamento
\emph{intelectual}. Resumidamente apontaríamos para a convergência entre
o compromisso intelectual, o compromisso universitário e o compromisso
crítico: se essa tríade nunca se realizou harmoniosamente, ao menos se
pode dizer que houve um tempo em que permanecia como um horizonte
regulador ao qual os universitários --- professores e alunos ---
remetiam seus projetos. Com a vitória da tecnocracia e da tecnociência ,
já não há mais lugar sequer para a tensão entre essas formas de
compromisso, porque nenhum deles pode apresentar hoje densidade
suficiente para respaldar uma conduta. ``Durante décadas, os
\emph{experts} coexistiram com os intelectuais antigos: uns desconfiavam
com razão dos outros. Hoje a batalha parece ganha pelos \emph{experts}:
nunca se apresentam como portadores de valores gerais que transcendam a
esfera de sua expertise e, em conseqüência, tampouco se encarregam dos
resultados políticos e sociais dos atos fundados
nela.''\footnote{\versal{SARLO}, Beatriz. \emph{Escenas de la Vida Postmoderna}, apud \versal{AVELAR},
Idelber, ob. Cit., p\,102.}  Não é necessário o domínio total e
explícito da tecnoburocracia e da tecnociência para que a universidade
seja definida pela hegemonia dos \emph{experts}: trata"-se de um modelo
de conduta, que cada vez mais aparece, não apenas como o mais adequado,
mas também como o único possível: aceitação acrítica de diretrizes
superiores, adequação a expectativas geradas pelo privilégio dos
indicadores formais, ausência de reflexão política nos planos e nas
avaliações, concentração de esforços na eficiência funcional, redução
ou, se possível, eliminação das oportunidades de debate institucional,
desagregação dos fóruns de reflexão coletiva e desencorajamento das
iniciativas de reflexão crítica individual, permuta de apoios por
benefícios, são algumas das características já bem implantadas na
universidade e que nos permitem afirmar o processo de metamorfose da
instituição em organização técnica de treinamento para o mercado
globalizado.\footnote{Deixamos de lado aqui um fator de extrema importância e que mereceria um
desenvolvimento à parte: a organização cada vez mais empresarial e
administrada da pesquisa, que vai fazendo desmoronar o que até há pouco
tempo era o último reduto do espírito universitário.} 

Ao tomar a direção da unidimensionalidade a universidade não faz mais do
que acompanhar a história e a sociedade, principalmente na América
Latina em que os governos ditatoriais encarregaram"-se de preparar a
transição para a hegemonia do mercado, trabalho consolidado pelos
liberais e pela ``esquerda'' convertida ao credo economicista. Ora a
unidimensionalidade oculta completamente as contradições porque não
admite qualquer tensão no indivíduo e na organização social. A~linearidade e a homogeneidade, implantadas por vários meios que vão
desde o terror até o império da mídia, fizeram com que a universidade
enquanto organismo crítico perdesse o lugar social, que aliás nunca lhe
tinha sido outorgado de boa vontade, mas que ela assumia como seu papel
histórico. As crises porque passou a universidade sempre foram
ocasionadas pelo acirramento de contradições latentes: se a instituição
se debilitava no embate de suas diferenças, ela por outro lado se
fortalecia ao fazer disso mesmo a sua diferença. Assim se pode dizer
que, se a universidade não sucumbiu às suas próprias contradições, é
quase certo que ela morrerá vitimada pela sua pacificação, visto que
esta é atualmente a grande violência que sofre, mas à qual não pode
resistir porque ela mesma incorporou essa violência ao subordinar o
pensamento à tecnificação e ao consenso pragmático.

Franklin Leopoldo e Silva



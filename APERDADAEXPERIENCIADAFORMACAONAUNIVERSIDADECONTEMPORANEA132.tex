\chapter{A perda da experiência da formação na universidade
contemporânea}

Por ocasião de um debate radiofônico com Hemut Becker sobre as
finalidades da educação, em 1966, Adorno definiu a educação como
``\emph{a produção de uma consciência verdadeira}''\footnote{\versal{ADORNO}, T.W\,Educação --- para quê? \emph{in}  \emph{Educação e Emancipação}.
Trad. brasileira de W.L\,Maar, Paz e Terra, Rio de Janeiro, 1995, p\,141.} . A~princípio, seríamos tentados a considerar que a generalidade da
definição a torna quase anódina. Ela parece respeitar todos os termos e
condições em que tradicionalmente se equacionou o problema da educação.
Pois desde o cuidado socrático com a alma estamos habituados a ouvir e a
ler que a pedagogia tem a função de conduzir a consciência individual a
apropriar"-se da verdade acerca de si e acerca do mundo. Mas o contexto
em que essa definição é proferida nos leva a interpretá"-la muito mais
como o enunciado de uma questão do que como uma afirmação peremptória.
Com efeito, há uma enorme diferença entre falar de \emph{consciência} e
de \emph{verdade} num contexto histórico de pensamento em que essas
noções encontram"-se elaboradas dentro de um sistema de idéias ordenado e
coerente, em que os pressupostos adotados respondem pela consistência
interna da totalidade e das relações entre todos os elementos que a
compõem; e enunciar essas mesmas noções numa época histórica em que elas
estão afetadas por um intenso processo de dissolução. Ora, é esse último
contexto que caracteriza a situação a partir da qual se pode falar, na
atualidade, de consciência e de verdade.

Vivemos o paradoxo histórico, que o próprio Adorno tentou reiteradamente
compreender e que constitui o eixo de seu pensamento crítico, de uma
herança civilizatória que desmentiu --- ou traiu \mbox{---,} no decorrer de sua
própria constituição, os pressupostos implicados na sua gênese. Aquilo a
que poderíamos chamar a proposta da modernidade, como sabemos,
orienta"-se por um horizonte em que a emancipação da razão deveria
produzir, como frutos de um conhecimento fundado em bases exclusivamente
racionais, a perfeita integração entre o saber e a ação, a teoria e a
prática, do que resultaria a realização humana em todos os aspectos,
isto é, a consecução de todos os fins humanos, como preconizava
Descartes. O~humanismo moderno, na sua origem, concebia muito
naturalmente a consolidação do império do homem como sendo também, e
necessariamente, a efetivação da sua felicidade. Essa idéia de um ser
humano completo, completamente realizado, não é apenas a versão laica da
bem"-aventurança, mas é principalmente a afirmação da liberdade da
consciência como o fundamento da verdade que agora aparece para o homem
no plano daquilo que pode atingir por si mesmo. Importa enfatizar,
portanto, a vinculação, no humanismo clássico, entre \emph{consciência,
verdade e felicidade}. E~desde já podemos notar que no enunciado pelo
qual Adorno define a educação, a consciência e a verdade estão
vinculadas, mas a felicidade está ausente. Compreenderemos a diferença
entre o contexto das origens modernas do humanismo e aquele que vivemos
contemporaneamente se entendermos que essa ausência não é, de forma
alguma, acidental. Trata"-se de um vazio que constitui a nossa
atualidade, que portanto nos constitui enquanto herdeiros históricos de
uma certeza que se revelou uma esperança perdida.

Não podemos certamente recuperar essa esperança, mas podemos ao menos
tentar compreender as causas que a tornaram frustrada. Na
impossibilidade de um exame mais pormenorizado dessas causas, diremos
apenas, alertando para a inevitável simplificação, que o processo de
desenvolvimento da razão emancipada --- aquela que se pretendia como
sustentáculo de um equilíbrio perfeito entre a teoria e a prática ---
provocou efeitos civilizatórios contrários aos seus pressupostos,
exatamente pela impossibilidade de manutenção desse equilíbrio entre a
razão como \emph{meio} da produção de instrumentos científicos e
técnicos de aprimoramento da civilização, e a mesma razão como
discernimento dos fins humanos a que tais instrumentos deveriam servir,
para o efetivo aprimoramento da vida. É~precisamente esse desequilíbrio,
e a consequente prevalência dos meios, isto é, da razão puramente
instrumental, que nos coloca hoje na posição, impensável para um
humanista clássico, de poder enunciar a pergunta: progresso --- para
quê? Isso significa que a separação entre meios e fins, que na origem
tinha o propósito de permitir a articulação das duas instâncias na
unidade da razão, tornou"-se um isolamento e uma desconexão total e
absoluta entre meios e fins, o que tende a fazer da racionalidade
técnica e instrumental uma força cega, empenhada numa trajetória que
acabou por fazer de si própria a única referência de percurso.

É evidente que essa quebra na unidade originária dos dois polos de
racionalidade --- o teórico e o prático --- bem como a progressiva
intensificação de uma nova articulação, não apenas dividiu a
consciência, como também desencadeou um processo, que parece estar bem
adiantado, de dissolução da dimensão da subjetividade em que o homem
poderia reconhecer a autonomia de suas ações e o próprio teor humano,
individual e comunitário, dos fins a serem perseguidos. O~que significa
que o processo de autonomização racional produziu um fenômeno de
\emph{unilateralidade}: a perda da dimensão prática, ética, ativa, da
subjetividade. Trata"-se da situação, atualmente vivida, da perda das
referências éticas, ou da substância ética da vida. Deve"-se entender
isso como unilateralidade porque não se trata apenas de uma divisão ou
de uma tensão entre elementos diversos; o que ocorre é uma anulação da
relação ético"-prática com o mundo, o que resulta numa anulação da
subjetividade, já que o \emph{sujeito} perdeu o equilíbrio que o
sustentaria na articulação entre meios e fins, e dessa maneira perdeu a
possibilidade de produzir a \emph{verdade histórica} que preencheria a
existência com um sentido efetivo. Daí as expressões ``crise de
sentido'' e ``crise de valores'' que são muitas vezes utilizadas para
designar a época contemporânea.

Vivemos portanto numa época caracterizada pela dissolução da consciência
e pela dissolução da verdade. É~a partir dessa constatação que devemos
procurar entender o significado da definição de Adorno: a educação é a
\emph{produção de uma consciência verdadeira}. Tanto mais que, na
explicitação dessa definição, Adorno identifica o caráter verdadeiro da
consciência com a sua \emph{emancipação}: ``Isto [a produção de uma
consciência emancipada] seria inclusive da maior importância política;
sua idéia, se é permitido dizer assim é uma exigência política. Isto é:
uma democracia com o dever de não apenas funcionar, mas de operar
conforme o seu conceito, demanda pessoas emancipadas. Uma democracia
efetiva só pode ser imaginada enquanto uma sociedade de quem é
emancipado.''\footnote{\versal{ADORNO}, T.W\,Ob. Cit., pgs. 141--2.}  A questão surge, então, colocada em toda
a sua contundência: como se pode falar de consciência emancipada
precisamente num contexto histórico em que as condições objetivas
provocam a dissolução da subjetividade? Como educar para a emancipação
se as determinações sociais e históricas pressionam no sentido da
anulação do sujeito enquanto agente consciente e livre?

A bem dizer, não haveria resposta para essas questões, em sentido
absoluto. Mas a \emph{mediação política} que o texto de Adorno parece
estabelecer entre educação e emancipação pode talvez nos indicar uma
direção. A~emancipação é uma ``exigência política'' da sociedade
democrática. No entanto, a correspondência entre democracia e
emancipação pode permanecer uma relação apenas formal. Quando a
democracia não ``opera conforme seu conceito'' a sociedade não apenas
parece prescindir da emancipação como até mesmo produz uma pressão sobre
os indivíduos no sentido de que a emancipação não se torne uma
realidade. Há portanto, um componente antidemocrático nas sociedades
formalmente democráticas, que as condições conjunturais podem exacerbar,
a ponto de se produzir algo muito próximo de uma situação totalitária no
interior da própria democracia. Isso acontece quando os indivíduos
assimilam o que Adorno chama de ``ideais exteriores'' sem que estes
passem pelo crivo crítico de uma consciência emancipada. Ora, longe de
ser exceção, isso é antes a regra. É~uma decorrência do ``mundo
administrado'', isto é, de um mundo organizado ideologicamente, no qual
se perdeu a possibilidade de visão"-de"-mundo no sentido teórico, em que a
ideologia exerce uma pressão que parece não deixar qualquer interstício
para uma conduta emancipada da consciência. Nesse sentido, quando se
fala em emancipação, não se pode deixar de ``levar em conta o peso
imensurável do obscurecimento da consciência pelo
existente.''\footnote{\versal{ADORNO}, \versal{TW}. Ob. Cit., p\,143.}  Qual é a causa desse obscurecimento? Ela
pode ser encontrada, segundo Adorno, numa atitude de hiper"-realismo que
leva as pessoas a entenderem que a única conduta coerente perante a
realidade (o existente) é a \emph{adaptação}.'' Se posso crer em minhas
observações, suporia mesmo que entre os jovens e, sobretudo, entre as
crianças, encontra"-se algo como um realismo supervalorizado --- talvez o
correto fosse: pseudo"-realismo --- que remete a uma cicatriz. Pelo fato
de o processo de adaptação ser tão desmesuradamente forçado por todo o
contexto em que os homens vivem, eles precisam impor a adaptação a si
mesmos de um modo dolorido, exagerando o realismo em relação a si
mesmos…''\footnote{\versal{ADORNO}, \versal{TW}. Ob. Cit., p\,145.}  A introjeção de ``ideais
exteriores'' tornou"-se um componente da vida em sociedade, no mundo
administrado, de modo que a autopreparação para a adptação, e,
sobretudo, a idéia de que a sociabilidade significa sempre adaptação,
não permite que as pessoas notem que esse processo agride a consciência,
por ser essencialmente anti"-emancipatório. Trata"-se da concepção de que
a realidade é intocável e imutável: é este o sentido do hiper"-realismo,
a aceitação pura e simples à qual se segue naturalmente a adaptação,
\emph{como se não fosse possível agir de outra maneira, já que a
realidade por sua vez também não pode se apresentar de outra forma}.

É desse modo que a consciência se produz a si mesma como falsa,
naturalizando a sua relação com o mundo. É~próprio do ser natural viver
de um único modo e sempre o mesmo; a consciência humana tem como sua
diferença a prerrogativa da escolha, e quando nega essa diferença nega a
si própria. A~adaptação é portanto a maneira pela qual o sujeito
participa do processo objetivo que provoca a sua própria anulação, numa
espécie de cumplicidade encorajada pelas próprias condições históricas.
Nem é necessário observar que a educação tem muito a ver com essa
confluência entre o hiper"-realismo do sujeito e as determinações
históricas de um contexto que não pode suportar consciências
emancipadas. É~o processo de ajustamento, que dissimula nas suas
próprias formas de realização a essência autoritária que o define. O~que
resulta desse processo é um empobrecimento e uma desfiguração da
\emph{experiência}. ``A experiência é um processo auto"-reflexivo em que
a relação com o objeto forma a mediação pela qual se forma o sujeito em
sua `objetividade'.''\footnote{\versal{MAAR}, W.L\,À~Guisa de Introdução: Adorno e a Experiência Formativa. \emph{in} 
\versal{ADORNO}, \versal{TW}. Ob. cit., p\,24.} 

Nunca enfatizaremos suficientemente a relação entre \emph{sujeito,
experiência e formação}. Em primeiro lugar cabe observar que a
experiência é um processo de formação do sujeito. Este portanto não é
aquela substância metafísica que que Descartes julgou descobrir
refletindo sobre o procedimento de duvidar. A~menos que este próprio
procedimento de dúvida seja tomado como uma figura da relação mais ampla
entre a consciência e os objetos na qual por auto"-reflexão \emph{se
forma} o sujeito, isto é, se estabelecem as mediações por via das quais
o sujeito tomará consciência de si diante dos objetos. O~sujeito não
pode ser concebido separadamente de sua própria experiência e esta só
pode ser concebida como o dinamismo de formação do sujeito por via de
sua auto"-reflexão. Não há, portanto, qualquer modelo ideal e exterior de
experiência, assim como não pode existir um paradigma de constituição da
subjetividade. Mas há um horizonte regulador com o qual podemos
confrontar a cada momento a experiência de formação da subjetividade: é
a \emph{formação realizada}, isto é, a subjetividade plenamente
constituída na experiência --- o que em termos hegelianos seria a
``ciência da experiência da consciência''. Tal ponto jamais será
atingido porque o homem é um ser histórico e não lhe encontraríamos um
sentido se a história fosse finalizada. Portanto a formação como
processo de experiência de uma subjetividade que constantemente se
constitui na temporalidade histórica poderia ser entendido como o
sentido da emancipação. O~que nos impede de conferir à noção de
emancipação uma significação apenas conceitual e abstrata.
Concretamente, sabemos que a experiência de constituição da
subjetividade, que seria uma trajetória de emancipação do indivíduo, é
travada pelas condições sociais e históricas.

Mas como somos seres históricos e não naturais, somos consciências e, em
termos sartrianos consciência significa antecipação de si mesmo, então
podemos exercer a liberdade de negar a realidade presente, o que deveria
ser um momento dialético de nossa relação histórica com o mundo. Essa
negação, enquanto recusa de \emph{adaptação}, não substitui a ordem do
existente mas pode suspender criticamente o processo de ajustamento
derivado do realismo exacerbado. Negar a realidade presente não
significa a pretensão de anular o mundo (como uma forma de reagir à
anulação do sujeito) mas apenas compreender que a experiência inclui uma
relação transformadora com o objeto, o que vem a ser também uma
transformação do próprio sujeito. Em termos mais estritamente adornianos
seria a recusa do existente. Essa recusa é algo que se deve incorporar à
experiência, para que ela não degenere em adaptação e ajustamento, mas
possa se aproximar da \emph{formação}. Qual será a via dessa
aproximação? ``Mesmo correndo o risco de ser taxado de filósofo, o que,
afinal, sou, diria que, (…) hoje em dia, (…) a única
concretização efetiva da emancipação consiste em que aquelas poucas
pessoas interessadas nesta direção orientem toda a sua energia para que
a educação seja uma educação para a contradição e para a
resistência.''\footnote{\versal{ADORNO}, \versal{TW}. Educação e Emancipação. \emph{in}  Ob.cit., p\,182--3.} 

O que pode significar ``uma educação para a contradição e para a
resistência''? De acordo com o que vimos acerca do hiper"-realismo, as
pessoas são encorajadas a uma aquiescência total ao que existe, como se
fosse anti"-natural ou utópico ou insensato opor"-se àquilo que se impõe
como realidade. Essa atitude naturalista perante as coisas é tão
difundida porque corresponde a um dogmatismo que nem sequer é fruto de
crenças fortes, mas simplesmente desempenha uma função acomodadora. Ao
hiper"-realismo corresponde portanto algo como uma vontade de unicamente
afirmar, no sentido de corroborar sempre a realidade. É~a atrofia da
capacidade crítica, certamente, mas devemos compreendê"-la não apenas
como empobrecimento existencial e cultural mas também do ponto de vista
das condições objetivas, isto é, do clima de ``consenso'' naturalista
que rechaça qualquer atitude de contestação e de crítica assim que elas
ameaçam aparecer. Ora, o que se tem de considerar --- e a educação pode
desempenhar algum papel no desenvolvimento dessa postura --- é que tudo
que existe deve ser visto tanto pelo lado afirmativo da sua existência
quanto pelo lado negativo de que poderia não existir e poderia haver
outra coisa em seu lugar. Essa relativização do existente somente pode
ser operada por meio da negação. Negar não é suprimir pura e
simplesmente; é pensar que sempre é possível uma outra posição. A~realidade não é uma plenitude positiva e as coisas não existem de modo
absoluto. Ainda que se concorde com isso em teoria, na prática as
pessoas se comportam como se tudo fosse necessário, e essa lógica, que
seria imanente às próprias coisas, pode justificar tudo. Esse é o
aspecto perigoso do realismo exacerbado e da adaptação. Por isso a
contradição do existente (no sentido de contradizê"-lo) é uma atitude que
aponta para a emancipação porque produz a resistência àquilo que é
imposto como opção única de realidade. É~o que vimos antes como a recusa
do existente.

Essa atitude não é gratuita e nada tem a ver com um exercício da
oposição simplesmente pela oposição. É~algo que repõe nossa consciência
no movimento que lhe deveria ser próprio, o da ação lastreada
historicamente. Pois para um ser histórico, a abertura de possibilidades
é sempre histórica. É~nesse sentido que o passado histórico, por ex.,
adequadamente elaborado, pode sustentar a crítica do presente, não
porque uma tal crítica se confunda com nostalgia ou desejo de volta ao
passado, mas porque as possibilidades do passado, tanto as realizadas
quanto as não realizadas, nos ajudam a ampliar e aprofundar os critérios
de consideração do presente. Walter Benjamin mostrou esse componente de
irrealização na história, que de alguma maneira deve ser recuperado numa
visão não positivista do curso histórico. Mas é próprio do
hiper"-realismo a exaltação do presente, que por sua vez deriva da
ideologia do progresso. É~isto que faz com que a atitude crítica seja
confundida com nostalgia, conservadorismo, catastrofismo, paranóia,
utopia, desatualização etc. Quando se considera a história apenas para
exaltar o presente e a linearidade necessária do progresso, de fato se
recusa a história, porque se deixa de ver que, no seu movimento
dialético, muitas vezes a história realiza possibilidades na forma da
própria negação delas. Como compreender de outra maneira que as
possibilidades emancipatórias que o humanismo moderno continha na sua
origem foram negadas no desenvolvimento histórico, e negadas pelos
próprios meios que deveriam servir à sua realização, ou seja, aqueles
implicados na autonomia da razão? Como explicar que o progresso da
tecnologia e as revoluções industriais, o acervo impressionante de
descobertas que a ciência acumulou no espaço de quatro séculos tenham
contribuído para fazer desmoronar as promessas de felicidade e para
tornar opaco o futuro no que se refere à realização das finalidades
humanas? Sem a consideração dessas contradições tão incrustadas na vida
histórica, não há como compreender a \emph{experiência humana} nos
termos da sua realidade e das suas possibilidades. E~assim não há como
compreender também a constituição do humano como experiência de
formação, ainda que tivéssemos de compreendê"-la como experiência
abortada.

Pois o que se pode depreender do que foi dito é que estamos diante de
uma experiência \emph{arruinada}. A~conformação histórica tomada pela
modernidade, ao truncar a experiência, descaracteriza a formação do
sujeito ao interferir decisivamente --- e negativamente --- no processo
de auto"-reflexão. A~impossibilidade de auto"-reflexão configura a
heteronomia do sujeito, pois tudo se passa como se o sujeito vivesse uma
experiência alheia a si próprio. É~a isso que se costuma chamar
``experiência da alienação'', expressão que, no rigor dos termos, é
contraditória, se admitimos que o sentido verdadeiro de
\emph{experiência} é inseparável de \emph{auto"-reflexão formadora},
requisito da autonomia. É~importante enfatizar a noção de
\emph{experiência} na sua vinculação à \emph{formação} para não
sucumbirmos à tentação de entender a impossibilidade aqui descrita como
a perda de algum ``ideal formativo'' que se teria desgastado nos
avatares da história cultural. Não se trata de admitir um ideal não
realizado, um princípio absoluto de orientação ao qual a experiência
histórica não teria conseguido ser fiel. Esse platonismo comprometeria a
experiência humana com a heteronomia no próprio processo de sua
constituição, devido à prevalência de um paradigma externo. As
experiências humanas moldam a história e a história molda as
experiências humanas: não se trata de um círculo vicioso, mas da própria
definição de historicidade, ou da existência como vida histórica, cujo
sentido está na relação dialética que a liberdade mantém com as
determinações. De modo que devemos compreender que a \emph{experiência}
da subjetividade, da formação e da autonomia se constitui ou se
desconstitui no rumo histórico da humanidade e a partir de condições
objetivas.

E isso coloca o pensamento crítico diante de uma inevitável ambiguidade.
Quando esse pensamento se liberta da remissão da finitude a ideais
absolutos abre"-se o campo de possibilidade para uma crítica imanente,
uma vez que o pensamento e a ação passam a ser vistos na contingência do
processo histórico. Ao mesmo tempo como o processo histórico realiza
alguns possíveis e deixa outros sepultados ao longo do seu próprio
percurso, a história efetiva aparece como que regida por uma necessidade
imanente. Pois quando o acontecimento se torna passado, quando o
presente já aconteceu, os possíveis que orbitavam em torno da realidade
também se tornam passado. De que adianta perguntarmos, a partir do já
ocorrido, como as coisas \emph{poderiam ter ocorrido}? É dessa forma que
se encoraja uma \emph{explicação} totalmente positiva do passado
histórico --- e da história --- que no entanto não é uma
\emph{compreensão} da história, porque deixa de lado exatamente o
conflito de possibilidades que está no engendramento de cada presente.
Já vimos como isso está na origem do fenômeno de adaptação enquanto
aceitação absolutamente realista do presente, uma \emph{conformação} à
``objetividade'' do presente histórico, atitude \emph{conformista} que
procura ignorar como o presente \emph{se formou}. A \emph{compreensão}
da história, não no sentido de Dilthey, mas no sentido dialético,
permite que o pensamento crítico se exerça de maneira consciente da
ambiguidade a que nos referimos. Pois não preciso ascender até ideais
transcendentes para escapar da aparente necessidade imanente ao processo
histórico, se considerar essa necessidade como o triunfo de
possibilidades tão contingentes quando aquelas que deixaram de se
realizar. Esse componente de \emph{irrealização} que se conserva na
realização histórica nos permite relacionar a cristalização do passado
com a liberdade dos agentes históricos que o construíram como presente.
Ou seja, a consciência histórica tem que operar negativamente se quiser
compreender que a história não são os \emph{dados históricos} mas a
experiência humana que nela se formou.

É essa compreensão que \emph{forma} o sujeito histórico, isto é, que
torna a sua \emph{consciência verdadeira}. Ora, se a formação tem algo a
ver com a educação, essa relação só pode se dar com uma \emph{educação
crítica}, uma educação que subverta os padrões adaptativos impostos pela
desagregação histórica da experiência. A~formação significa, entre
outras coisas, a possibilidade de o sujeito articular"-se historicamente,
ou seja, equilibrar a sua inserção no presente a partir de uma
articulação entre o presente e o passado históricos. Já vimos que não
pode haver crítica do presente se ele for considerado absoluto, e se o
futuro só puder ser considerado como uma extensão meramente quantitativa
do tempo presente. A~apropriação crítica do presente é inseparável da
sua relativização.

``A educação crítica é tendencialmente subversiva. É~preciso romper com
a educação enquanto mera apropriação de instrumental técnico e
receituário para a eficiência, insistindo no aprendizado aberto à
elaboração da história e ao contato com o outro não"-idêntico, o
diferenciado.''\footnote{\versal{MAAR}, W.L\,À~Guisa de Introdução: Adorno e a Experiência Formativa. \emph{in} 
Ob. Cit., p\,27.}  A questão que se põe a seguir é tão
difícil quanto é clara e coerente a conclusão a que chega o texto
citado: é possível que a educação institucionalizada venha a assumir
essa perspectiva? A princípio essa pergunta parece ser uma daquelas que
alguém só faz porque já sabe antecipadamente a resposta. Mas é de se
supor que a questão encerra muito mais do que um efeito retórico. Se a
perda da possibilidade da experiência formativa é ``uma tendência
objetiva da sociedade'', correspondendo ao seu ``próprio modo de
produzir"-se e reproduzir"-se''\footnote{\versal{MAAR}, W.L\,Ob. Cit., p\,26.} , e se tal tendência é
acentuada no estágio atual do capitalismo, talvez não se possa esperar
dos agentes históricos uma reversão do processo. O~interesse
historicamente estabelecido a partir da necessidade de dominação pela
administração das consciências jamais poderia aceitar uma educação
crítica. A~situação é tanto mais complexa e grave quanto esse interesse
não atua na forma de uma determinação bruta e explicitamente repressiva,
mas de maneira insinuante e pela introjeção de padrões que os indivíduos
acreditam serem os modos espontâneos de relação entre consciência e
mundo. A~mercantilização da educação é, entre nós, a prova mais viva do
êxito dessa estratégia, pois significa que a sociedade já aceitou
inteiramente que a educação é um \emph{produto}, com o qual devemos
estabelecer a mesma relação de objetividade formal que estabelecemos com
todos os outros \emph{produtos e serviços} que são oferecidos para
consumo. E~o produto educacional universitário é justamente aquele que
poderia realizar de forma mais conclusiva a adaptação do indivíduo ao
mundo presente e ao mundo futuro pensado como extensão do presente. A~ampliação do comércio educacional, que nos últimos anos alcançou entre
nós proporções nunca vistas, atinge principalmente o segmento
universitário, haja vista a inflação de universidades e centros
universitários privados.

Porém quando falamos em ``educação institucionalizada'' parece que
habitualmente ainda queremos nos referir à escola pública, por via de
uma ligação mais forte, que o passado provavelmente legitimou, entre o
\emph{público} e o \emph{institucional}. Deveríamos então fazer uma
diferença entre instituição pública educacional, principalmente a
universidade, e as empresas privadas de educação universitária,
entendendo que o interesse historicamente estabelecido, a que nos
referimos há pouco, atuaria com menos intensidade na universidade
pública, a qual, justamente por ser \emph{pública}, refletiria de forma
menos imediata os objetivos pragmáticos que as organizações privadas
proclamam tão abertamente? Em suma, o espaço de pensamento crítico que
permite a formação estaria ainda preservado, de alguma maneira, na
universidade pública? Creio que se deve responder negativamente a essa
pergunta, mas, para prevenir a acusação de radicalismo ou de
catastrofismo, convém explicitar as razões da resposta negativa.

O argumento tradicional para a convivência do ensino privado com o
ensino público consiste em afirmar que, numa democracia, as pessoas
devem ter o direito de escolha e de exercer opções dentro dos limites
legais. É~o que fundamenta a liberdade de crença religiosa, de adesão
política, de trabalho etc. A~educação estando entre as possibilidades de
opção, é necessário que exista a escola privada como alternativa ao
ensino público, para contentar àqueles que desejam optar por educar seus
filhos ou educar"-se numa escola particular. Essa seria a razão da
\emph{convivência} de dois sistemas. A~convivência democrática supõe que
a existência dos dois elementos nunca poderá chegar a um grau de
diferenciação e supremacia que resulte em ameaça de extinção de um
deles. Todos nós sabemos com que ardor os donos de escolas privadas
defendem este princípio democrático. O~problema é que o poder público
não o defende com o mesmo entusiasmo. O~desinteresse pela educação,
manifestado sobretudo pela exiguidade de recursos mas também,
principalmente nos últimos tempos, pela obsessão punitiva contra aqueles
que se dedicam ao ensino público, demonstra a evolução de um projeto
deliberado de desestabilização do sistema público de ensino, mormente o
universitário, o que objetivamente significa beneficiar o ensino
privado. Há no entanto outro ponto, que afeta ainda mais a universidade:
o modelo privatista de organização e gestão que vem sendo implantado já
há muito tempo, sob pretexto da eficiência e da produtividade, e que
tende a dissolver a diferença entre instituição pública e organização
privada. Ora, se supomos que finalidades específicas só podem ser
cumpridas se associadas a meios adequados a esta especificidade, o
quadro que se desenha diante de nós é o de uma escalada progressiva na
direção da inviabilidade das atividades acadêmicas tais como se
configuram numa instituição pública universitária. O~desequilíbrio
gerado pela imposição do modelo privatista da relação custo/benefício e
da eficiência refletida nos resultados imediatos desmente na prática o
princípio democrático da convivência entre o público e o privado. São
essas as razões que mostram, a quem quiser ver, que existe uma ameaça
objetiva à universidade pública, que em princípio e ao menos
teoricamente, poderia ainda ser pensada como possibilidade de formação.
As transformações que lhe vão sendo impostas --- e muitas já se
consolidaram --- implicam na anulação das condições acadêmicas para o
exercício do pensamento crítico.

Trata"-se, como vimos, de um processo histórico que deve ser pensado a
partir de suas condições objetivas. Mas trata"-se também de uma adaptação
cega ao presente histórico e, neste sentido, de uma cumplicidade que os
sujeitos históricos mantêm com as condições objetivas, que de forma
alguma precisariam ser interpretadas como a necessidade inelutável. A~causa de que haja atualmente tamanho empenho, dentro e fora da
instituição, na destruição da universidade pública, certamente pode ser
pensada como uma figura privilegiada da confluência entre dois elementos
interdependentes: o desprezo pela responsabilidade histórica e a
desagregação moderna da integridade da experiência humana.



\chapter{O futuro da universidade}

O futuro da universidade: na abordagem desse tema faz"-se necessário
antes de tudo uma precaução, no sentido de procurar evitar muito do que
se tem dito ultimamente acerca da ``universidade do futuro'', a
``universidade do terceiro milênio'', a ``universidade do novo século''
etc. Quase todas as vezes que tais expressões são utilizadas pode"-se
perceber um propósito de enaltecer no presente os elementos do futuro
que nele são anunciados. Perspectiva que coloca o problema de como
devemos considerar o futuro: será o futuro o resultado da inserção de
nossas ações numa temporalidade linear, em que o curso evolutivo dos
eventos vai fazendo aparecer novas paisagens que o tempo nos reservaria,
assim como vamos descobrindo progressivamente as coisas que se situam
além do horizonte nos caminhos percorridos? Ou será o futuro aquilo que
projetamos a partir de nossas expectativas, que nascem, confirmam"-se,
modificam"-se ou se desfazem no confronto dialético entre nossas
esperanças e os eventos concretos que encontramos e vivemos à medida em
que o futuro se faz presente? Talvez a simples visão retrospectiva da
experiência baste para nos convencer de que a temporalidade histórica
não deveria ser pensada como uma trilha em que a sucessão fosse
constituída por marcas de orientação, cuja diversidade e variedade de
localização não superaria a unidade de direção, já desde sempre
previamente traçada por quaisquer ordens de determinação. Viveríamos
assim situações em que as mudanças trazidas pelo futuro seriam sempre
perfeitamente compatíveis com a continuidade do presente, e poderíamos
depositar uma confiança irrestrita nesse fundo de homogeneidade sobre o
qual ocorreriam as diversificações do fluxo do tempo. A~experiência,
sobretudo no seu aspecto histórico, mostra que o fluxo do tempo é muitas
vezes permeado por contradições, imprevisibilidade e decepções, o que
nos impede de viver o tempo como uma série de apropriações contínuas de
resultados sucessivos.

No entanto, muitas vezes confundimos a \emph{inexorabilidade da passagem
do tempo} com a \emph{inevitabilidade dos eventos que se sucedem}, como
se todos os acontecimentos devessem ser compreendidos em função de uma
história presente que os constrói, por assim dizer preparando"-os para se
tornarem reais. É~isso que nos faz privilegiar o presente, como tempo de
construção e preparação do acontecimento futuro. Diríamos, numa
linguagem analítica, que o futuro, como \emph{efeito}, somente pode
conter aquilo que já está contido em sua causa no presente. E~procuramos
reordenar as diferenças que nos decepcionam tentando encontrar,
retrospectivamente, identidades que eventualmente não teríamos percebido
no presente vivido, mas que lá teriam existido de qualquer forma,
orientando o advento do futuro, exatamente como a causa produz, por si
mesma, o efeito. Isso significa que quando valorizamos o futuro estamos
na verdade enaltecendo o presente, que já o conteria em princípio. Se o
curso do tempo é necessário, por que não o seria também cada evento que
confere à temporalidade a realidade historicamente efetiva?

Isso pode nos ajudar a compreender porque muitas vezes o futuro da
universidade é tão precisa e seguramente afirmado como a realidade da
universidade do futuro, cujos traços já poderíamos distinguir no
presente com suficiente nitidez. Com efeito, daquilo que a universidade
é, no momento, como realidade presente, podemos extrair elementos para a
representação de como ela será no futuro, visto que quanto mais certas
características se afirmam no presente, tanto mais elas tendem a se
prolongarem em continuidade no futuro. Daí a afirmação que, no que se
refere à universidade assim como em muitas outras realidades
institucionais e políticas, o caminho é irreversível, pois do fato de
que não podemos tornar o presente imediato diferente do que ele é se
poderia inferir a necessidade de um futuro moldado no presente e que sem
dúvida aprofundaria suas características, admitindo"-se a lei de
continuidade do progresso. Deveríamos portanto aceitar com naturalidade
tanto o presente, fruto do passado, quanto o futuro, resultado do
presente. Concebida dessa forma a experiência do tempo histórico, só é
possível uma atitude: a adaptação como conformidade racional ao que
existe e ao que virá a existir.

Certamente isso tem muito a ver com uma concepção evolucionista da
experiência histórica. Assim como a evolução considera o progresso na
cadeia de seres vivos medindo"-o pelo grau de adaptação ao meio ambiente,
assim também o progresso civilizatório poderia ser estimado em relação à
adaptação dos indivíduos ao ``meio'' histórico: daí o valor que se
costuma conferir àqueles indivíduos que sabem ``responder'' às
necessidades de sua época, que se colocam ``à altura'' do contexto
histórico em que vivem. Contrastando com estes, teríamos aqueles que não
sabem reconhecer as solicitações de seu tempo e que, assim, não
``correspondem'' ao progresso. Essa concepção naturalista da história
está por trás da passividade que atualmente é requerida como a única
conduta coerente, aquela que se pauta pela concordância fundamental com
o que nos é imposto como o curso natural das coisas. O~grande mérito do
conhecimento seria o de antecipar as etapas dessa evolução, para que
pudéssemos orientar nosso comportamento presente pelas representações
que fazemos desse futuro em vias de acontecer. Essa seria a diferença
entre os seres simplesmente naturais e o homem: este \emph{sabe} que
vive sob a égide do \emph{progresso natural}, e essa consciência lhe
permite preparar"-se eficazmente para as etapas subsequentes,
reconhecidas por via da capacidade de previsão.

Mas se ainda não perdemos totalmente a capacidade de discernir entre os
seres naturais e os seres humanos, e se ainda não estamos de todo
submetidos a uma visão reificada da experiência, talvez possamos
suspeitar que o que configura a singularidade humana não é tanto a
resposta às injunções do meio natural mas principalmente a capacidade de
agir em função das condições históricas no confronto com as quais se
constrói o estatuto de agente histórico. Como tais condições são geradas
num contexto humano, o conflito entre o sujeito e a realidade não
significa necessariamente a morte causada pela desadaptação, mas a
possibilidade de transformação que é, propriamente, a característica
distintiva da experiência histórica. Isso significa que não existe uma
ordem natural inelutável definida como evolução e progresso: existem
resultados contingentes da ação histórica, que são incorporados à
representação da realidade configurando assim a experiência humana na
sua generalidade, composta do heterogêneo e do contraditório. Tais
reparos nos previnem contra uma linearidade continuista e evolutiva que
faria da ação humana não uma \emph{experiência}, mas uma sucessão de
comportamentos naturais. E~essa precaução é tanto mais necessária quanto
podemos ver, no ideário da globalização e do neoliberalismo, uma
concepção radical de mecanicismo comportamental que dissolve as relações
entre subjetividade e história, entre a ação e a política, numa
pluralidade de reações, ou de reativações, inteiramente governada por
determinações a priori, destinadas a perfazer um consenso que visa menos
do que produzir um ``pensamento único'' porque objetiva a supressão do
pensamento por via da total abstração da realidade do sujeito,
inteiramente identificado ao elemento de um sistema. Dessa forma,
postula"-se uma definição de individualidade como concorrência puramente
vitalista, e a comunidade humana passa a ser concebida como uma multidão
de átomos girando no vazio, compondo"-se e decompondo"-se numa dinâmica
gerida por forças que os ultrapassam.

As considerações feitas até aqui referem"-se a um conjunto de ideias que,
nos últimos 15 anos, mas com mais intensidade nesses 6 últimos anos,
atingiu profundamente a universidade, a ponto de podermos dizer que ela
se encontra hoje diante de opções que dizem respeito ao seu mais íntimo
modo de ser\footnote{Acerca das representações da universidade, num sentido diferente mas
também convergente com o que dizemos aqui, cf. \versal{CARDOSO}, Irene. Imagens
da Universidade e os Conflitos em torno do seu Modo de Ser.
\emph{Revista \versal{USP}}. Número 25, março/abril /95.} . Para compreender a crise, que se procura
aliás por várias formas esconder, tentemos analisar alguns elementos da
representação de universidade que vem sendo progressivamente imposta e,
em grande parte, assimilada. O~primeiro deles diz respeito à relação
entre a universidade do presente e a universidade do futuro. O~que está
sob muitos dos argumentos que defendem o que se convencionou chamar de
``modernização'' e ``racionalização'' da universidade, ou seja, o modelo
da eficiência produtivista, é a ideia de que somente aqueles que aceitam
o que nos é proposto como universidade do presente podem compreender a
universidade do futuro. Pois o que o presente tem de melhor seriam os
seus traços de futuro. Assim, o processo de modernização pelo qual
estaria passando a universidade conteria em si uma representação
antecipada de um projeto, que se realizaria (ou melhor, que
inevitavelmente se realizará) na universidade do futuro. É~interessante
notar que essa maneira de relacionar o presente e o futuro da
universidade confere aos elementos do presente um alto grau de
positividade, derivado exatamente de estarem eles inscritos não apenas
na atualidade mas também na representação do futuro, imaginando"-se que
no futuro tais elementos se consolidarão na sua total efetividade, o que
ainda não ocorre no presente. Isso serve então para qualificar aqueles
que tomam posição critica em relação ao presente: seriam os que não
conseguem articular presente e futuro de modo a perceber a continuidade,
e o progresso que ela representaria. Como o progresso é uma espécie de
``dado natural'', a resistência ao presente enquanto recusa de aceitar
passivamente o futuro que ele promete torna"-se uma atitude descabida, e
mesmo irracional enquanto indicadora de irrealismo.

Outro elemento, estreitamente relacionado ao anterior, é a ideia de que
a ``universidade do passado'' estaria definitivamente superada.
Invoca"-se, para defender essa ideia, o argumento de que a universidade é
um produto histórico, devendo portanto acompanhar as modificações que
marcam o transcurso das épocas. Vistas as coisas dessa maneira, a
experiência histórica passada possuiria uma conotação eminentemente
negativa, servindo apenas para registrar, como ``dados'', o que a
universidade \emph{foi} nas diversas etapas de seu desenvolvimento. Nada
disso poderia se integrar ao presente, porque o desenvolvimento
histórico por definição ``supera'' as etapas que ele atravessa. Como se
trata de uma trajetória de progresso, é sempre o presente que conta,
conferindo"-se ao passado uma dimensão no máximo subsidiária, como
condição cronológica de realização do presente. Essa qualificação
sumária do passado como negativo nivela todos os acontecimentos da
história da universidade, impedindo"-nos de diferenciar, neles, as
\emph{ações} promotoras da emancipação e da autonomia daquelas que
coonestaram a subserviência, o autoritarismo e a repressão. A~experiência histórica é relegada como inócua e evocada apenas como
curiosidade. E~qualquer tentativa de iluminar o presente por via da
experiência passada é vista como retrocesso, arcaísmo ou nostalgia. Isso
explica um certo embaraço em lidar com o passado, nos momentos em que se
torna inevitável falar dele, como nos momentos de comemoração. A~tradição se transforma num fardo incômodo quando o presente se configura
como traição do passado em vários aspectos. Na impossibilidade de
decretar o total esquecimento, o que seria o mais conveniente,
procura"-se reinterpretar o passado para mostrar que o presente é sempre,
de qualquer forma, a realização mais perfeita da realidade e das
projeções do passado, ainda que as contradiga. Atualmente é possível
observar essa atitude na relação que as pessoas estabelecem com as
experiências políticas da universidade no passado. Como a universidade,
no presente, caracteriza"-se como totalmente despolitizada, é preciso,
para justificar esse estado de coisas, retirar o valor intrínseco dos
compromissos políticos assumidos no passado. Esses são recontados então
de diversas formas, que vão, no melhor dos casos, da constatação de
equívocos bem"-intencionados até a rememoração complacente de um
voluntarismo heroico e utópico. Um arco de representação que despoja o
passado político da universidade de qualquer sentido efetivo em termos
de inserção real na sua atualidade histórica.

Um terceiro elemento importante na representação atual da universidade
diz respeito à relação entre indivíduo e comunidade. A~estrutura interna
das instituições e as pressões das agências de fomento encorajam por
todos os meios a formação de grupos de pesquisa a partir de duas
alegações. Em primeiro lugar, de um ponto de vista da administração de
recursos, seria mais racional que as verbas fossem distribuídas entre
projetos aglutinassem competências em vista de resultados integrados do
que dispersadas entre pesquisadores individuais com propósitos bastante
diferenciados e metas particularizadas em função de trajetórias de
pesquisa singulares. Em segundo lugar, do ponto de vista acadêmico, a
desvinculação do pesquisador de seu lugar de origem --- departamento,
instituto, faculdade ou mesmo universidade --- permitiria organizar os
grupos por critérios que estariam acima dos vínculos institucionais,
desobrigando"-o dos compromissos com o núcleo acadêmico a que
naturalmente pertence e levando"-o a prestar contas de suas atividades a
instâncias com as quais estaria, de fato, economicamente vinculado, o
que retiraria da atividade de pesquisa a sua subordinação à corporação
acadêmica imediatamente mais próxima do pesquisador. Supõe"-se que esses
tipos de agregação e vinculação abrem horizontes mais amplos, e propicia
uma relação mais saudável entre o indivíduo e o grupo. Na verdade o
objetivo visado por essa reorganização é a desestabilização e, no
limite, o rompimento de uma relação mais orgânica entre o indivíduo e a
comunidade acadêmica. O~que se procura é substituir vínculos lastreados
na história e na convivência institucional por compromissos formais,
análogos a ligações contratuais, em que o pesquisador aloca seus
serviços, em troca de recursos e condições de trabalho, a um grupo ao
qual estaria apenas formalmente vinculado, com o qual não se relaciona
por sua história passada e no qual os interesses não surgiram nem se
desenvolveram comunitariamente. Um agregado de pessoas que possuem em
comum apenas a expectativa de resultados que justifiquem o investimento
de recursos. Ora, dizemos que o indivíduo e a comunidade estão
organicamente relacionados quando a liberdade individual se fundamenta
no compromisso comunitário, e a contribuição individual será tanto mais
efetiva quanto mais lastreada numa história compartilhada a partir da
qual são gerados os interesses comuns. Quando essa gênese ou formação é
substituída unicamente pela visão prospectiva de resultados, a
vinculação adquire um caráter instrumental que desmente qualquer
vivência de trabalho realmente comunitário.

Mas é evidente que o trabalho universitário não está sendo reorganizado
dessa maneira por acaso. Essa maior mobilidade dos indivíduos, que tem a
aparente pretensão de desburocratizar e flexibilizar os agrupamentos de
pesquisadores, destina"-se, ao que tudo indica, a quebrar a relação entre
indivíduo e comunidade acadêmica, retirando dessa relação os eixos que a
firmavam na ambiência histórico"-institucional. Com isso se esgarçam
também os vínculos de convivência que, sob o pretexto de alargamento,
são tornados abstratos. Não é preciso refletir muito para entender que o
objetivo é a destruição do sentido comunitário da experiência acadêmica,
frequentemente desmerecida sob o epíteto de ``corporativismo'', e o
consequente elogio do individualismo exacerbado e da competição. Pois se
o grupo formado apenas para o objetivo utilitário de captação de
recursos constitui uma falsa comunidade, é claro que os indivíduos que
dele fazem parte não atuam comunitariamente, apenas agregam os seus
interesses individuais para a consecução de uma finalidade específica,
estando cada um na condição de meio para que o outro atinja sua
finalidade, só eventualmente combinada com os fins perseguidos pelos
demais. Essa instrumentalidade recíproca, que se traduz apenas numa
cooperação formal, de um lado desgasta a individualidade, porque esconde
seus liames concretos com a comunidade e, de outro, impede que a
instituição seja pensada de forma relacionada com o bem comum, dela
mesma e da sociedade, uma vez que o espaço institucional fica reduzido a
uma arena de competição. Ao mesmo tempo, a disseminação dessa atitude
tolhe qualquer possibilidade de compreensão da atividade científica e do
trabalho intelectual como produção social, repousando sobre uma
responsabilidade coletiva que se manifesta numa pluralidade diferenciada
de formas.

Esse comentário descritivo de alguns elementos da representação de uma
universidade ``modernizada'' e ``racionalizada'' pode ensejar algumas
reflexões que levem a nos situarmos dentro do quadro atual. Seja"-nos
permitido lembrar algumas coisas que todos já sabemos. A~primeira delas
é a ligação existente entre os elementos que compõem a representação da
universidade e as formas vigentes de sociabilidade. Isso é importante
para que se ganhe alguma clareza quanto à modalidade de inserção do
trabalho universitário no conjunto da sociedade. Vivemos um contexto em
que a ordem econômica não apenas predomina na organização material da
sociedade como ainda projeta um ``simbolismo econômico'' (expressão de
Marschal Sahlins) que é ``estruturalmente determinante'' na cultura,
como se houvesse uma lógica simbólica cujos operadores seriam de ordem
econômica.\footnote{Cf. a respeito \versal{BRANDÃO}, C.R\,A~Educação como Cultura. São Paulo,
Brasiliense, 1985, p\,106.}  Isso quer dizer que a produção simbólica
está inteiramente assimilada à produção de bens e serviços, o que
demarca o lugar e a significação do trabalho intelectual. Essa é a razão
pela qual a dimensão cultural, portanto simbólica, do saber tende a ser
entendida como produção de \emph{coisas}, mais precisamente
\emph{produtos}, avaliados economicamente. Ora, esse traço da sociedade
burguesa traz em si um elemento de recusa do trabalho intelectual, que
se apresenta e atua no entanto como \emph{assimilação} do simbólico à
coisa. Como resultado desse processo são geradas duas atitudes: a
primeira é a de conformar"-se à reificação e aceitar o modelo produtivo
como parâmetro do trabalho intelectual. A~segunda é considerar que o
trabalho intelectual \emph{transcende} as determinações sociais e se
realiza de forma inteiramente autônoma. No limite, a primeira vê o
trabalho intelectual como epifenômeno da produção material de bens, e a
segunda o vê como a instância do incondicionado. O~que falta a ambas as
posições é o sentido da experiência integral do trabalho intelectual: a
perspectiva em que a sua inserção social aparece como
\emph{conflituosa}, o que se torna patente quando consideramos a questão
da inserção da universidade numa configuração histórica dominada por uma
ideologia que a recusa.

Para retomar o fio das considerações iniciais, podemos afirmar que tanto
o conformismo adaptativo quanto a transcendência que tende para a
alienação representam modos de relacionamento com o tempo histórico: no
primeiro caso uma inserção absoluta; no outro, uma recusa absoluta.
Seja"-nos permitido referir aqui a análise de uma experiência política
como inserção histórica, que dá um testemunho muito preciso do que
qualificamos há pouco como inserção conflituosa.

``Esse patrimônio cultural constituído pela Faculdade de Filosofia,
Ciências e Letras foi, desde a sua fundação, marcado por um traço
bastante singular: o de um descompaso e inconformismo com o seu tempo
histórico, tanto como núcleo de criação social formulador de um
pensamento crítico, quanto como sede de lutas políticas importantes,
tais como contra o fascismo --- e o Estado Novo --- a campanha pela
escola pública, a luta pela reforma universitária, nos seus vários
momentos \redondo{[…]} e a resistência contra a ditadura instalada em
1964. Exatamente em razão desse traço de inconformismo com os limites
impostos pelo seu tempo não foram poucas, ao longo de sua história, as
tentativas de seu silenciamento.''\footnote{\versal{CARDOSO}, Irene. Maria Antonia: o Edifício de número 294. \emph{in}  José
Roberto Martins Filho (org), \emph{1968 faz Trinta Anos}. Editora da
\versal{UFSCAR}/Mercado de Letras/ \versal{FAPESP}, 1998, p\,41. A~autora se refere à
Faculdade de Filosofia, Ciências e Letras da \versal{USP} (atual Faculdade de
Filosofia, Letras e Ciências Humanas), mais precisamente ao episódio que
ficou conhecido como ``A Guerra dos Estudantes'', conflito entre os
estudantes da \versal{USP} e da Universidade Mackenzie em 2 e 3 de outubro de
1968. A~partir desse fato, introduz importantes reflexões acerca da
questão da inserção histórica da \versal{USP}. Em que pese a referência singular,
o texto possui um alcance geral ao evocar o problema das relações entre
universidade, autonomia do trabalho intelectual, crítica e história.} 

Como toda experiência histórica, a trajetória da universidade está
permeada por contradições. No caso da \versal{USP}, por ex., sua fundação deu"-se
num contexto de luta política entre a ilustração paulista e as
oligarquias rurais que então dominavam a política nacional. A~\versal{USP}, e
notadamente a Faculdade de Filosofia, Ciências e Letras, espécie de
escola"-núcleo da nova instituição, deveria se constituir como celeiro de
formação de dirigentes políticos com uma visão de mundo mais avançada,
possiblitando assim a inserção efetiva da jovem república na modernidade
política. O~objetivo, ao mesmo tempo renovador e conservador, encontrou
vários obstáculos à sua realização, tanto do lado do autoritarismo
vigentes na época quanto do lado da ampliação crítica dos horizontes de
formação que, malgrado a intenção dos fundadores, resultou na
radicalização política cuja maior expressão foram os episódios que
marcaram o ano de 1968. Daí a necessidade de silenciar a instituição que
no entanto em princípio havia sido criada para expressar anseios de
progresso e emancipação. Importa reter desse exemplo que a experiência
histórica, nesse caso, produziu uma \emph{inserção histórica
conflituosa}, na medida em que a instituição não seguiu nenhuma das
direções apontadas pela história do seu tempo: não se tornou instrumento
de ascensão das novas elites nem se configurou como \emph{locus} de
defesa da ordem vigente. Ou seja, a exigência crítica inerente a um
projeto de reforma da estrutura de poder se transformou num
questionamento radical da ordem burguesa. Isso aconteceu porque a
experiência histórica da universidade produziu a consciência das
contradições, visando"-as num nível mais profundo do que o pretendido
pelos idealizadores da instituição.

É o teor dessa experiência histórica que está sendo intencionalmente
desprezado pelos enaltecedores do presente. E~a razão disso todos
sabemos: o paradigma produtivo da universidade não comporta um trabalho
internamente mediado pela atitude crítica. A~racionalidade
técnico"-instrumental dominante requer para seu funcionamento a
imediatez, a unilateralidade e a univocidade.

``Vinculada essencialmente ao presente, por uma racionalidade formal que
não somente a envolve mas que também caracteriza cada vez mais a cultura
contemporânea nas suas diversas manifestações, a universidade perde, por
isso mesmo, o distanciamento crítico em relação a esse presente e à sua
cultura.''\footnote{\versal{CARDOSO}, Irene. Op.cit., p\,46.} 

Um presente medido apenas por si mesmo tende a tornar"-se absoluto, e
assim não pode oferecer"-se ao pensamento com a relatividade compatível
com a crítica e o discernimento das coisas humanas. Por isso não se
admite que mediações reflexivas possam fazer parte do trabalho
universitário, já que tais mediações apenas retardariam a produção
cultural entendida como produção de bens e serviços. É~o \emph{compasso}
da contemporaneidade que exige uma inserção absoluta e acrítica. Ora, já
vimos que a experiência histórica, quando vivida e refletida nas suas
contradições, provoca o \emph{descompasso} entre a universidade e o seu
tempo. O~que nos é proposto, então, é uma inserção mecânica que traz no
seu próprio ato a justificativa da adaptação. Mas por ser a experiência
histórica produzida pelo confronto entre a realidade e o sujeito agente,
ela não pode, como já vimos, ser entendida como sucessão mecânica de
eventos. Nenhum acontecimento se reduz à materialidade dos seus
elementos: ele supõe sempre a intenção significativa dos sujeitos que o
vivenciam, e é nessa dimensão que se encontra propriamente o \emph{ato}
histórico. Ora, essa intenção que atua no acontecimento é inseparável de
uma ``relação significativa com o passado'', razão pela qual uma
``sensibilidade estritamente voltada para o presente'' só pode reduzir a
experiência histórica da universidade à ``funcionalidade
institucional''.\footnote{\versal{CARDOSO}, Irene. Op.cit., p\,47.}  Nessa funcionalidade a sincronia tem
que prevalecer obrigatoriamente, como condição da univocidade e da
uniformidade, porque a percepção da diferença na temporalidade histórica
traria consigo a necessidade de qualificar os conteúdos dessa
experiência, para não considerá"-los todos na mesma positividade de uma
continuidade fundamentalmente homogênea. A~prevalência da funcionalidade
leva a uma articulação superficial da experiência. Na superfície da
experiência temporal, o passado é apenas aquilo que já passou e cujas
significações não sobrevivem à atualidade efetiva. Mas as significações
do passado atuam no presente --- e temos uma espécie de contraprova
dessa relação no empenho com que se procura \emph{esquecer} certos
segmentos da experiência passada, exatamente para recalcar as
significações.\footnote{Cf. a respeito dessa questão \versal{CARDOSO}, Irene. Memória de 68: Terror e
Interdição do Passado. \emph{Tempo Social --- Revista de Sociologia da
\versal{USP}}, 2 (2), 2. Semestre. 1990.} 

O compromisso do presente com essa ``interdição do passado''
naturalmente tem muito a ver com um outro recalque: o da
\emph{atualização} do presente como \emph{processo} histórico e
\emph{ação} humana. Isso se reflete na universidade como a valorização
do \emph{produto} e do \emph{serviço}. O~que está feito, acabado, pronto
para ser vendido e consumido é o que importa, porque não se atenta para
os modos específicos do fazer como aquilo que confere singularidade ao
que se faz. Em outras palavras, não se considera a \emph{experiência do
trabalho} mas apenas a \emph{operação e seus resultados}. Daí a
prevalência do viés quantitativo nas avaliações. A \emph{universidade
operacional} considera o conhecimento não um fim, mas um meio de
inserção no mercado, ao modo das outras organizações. Como toda
organização empresarial, ela abstrai todos os elementos do contexto
social para reduzir as necessidades sociais às injunções do mercado. A~visão mercadológica do trabalho universitário faz com que a universidade
perca seu teor institucional, fenômeno que ocorre paralelamente à
redução da relação com a sociedade à relação com o mercado. Por isso a
universidade operacional cumpriria melhor o seu papel se fosse
organização privada ou ``organização social'' nos moldes já propostos
por defensores da ``modernização''. Na verdade, o gerenciamento
universitário já se faz cada vez mais nos moldes de gestão privada, sob
pretexto da necessidade de ``racionalizar'' e ``agilizar''. Isso é
sentido no cotidiano do trabalho de ensino e pesquisa, a cada dia mais
subordinado a controles operacionais e à pressão por resultados a curto
prazo, o que substitui a experiência do trabalho, dotada de um tempo
próprio, pela sucessão de adaptações no tempo acelerado que rege o ciclo
da produção e do consumo. O~que se solicita do pesquisador é que, como
um ``homem de visão'', situe"-se desde já no futuro, estando ``à frente
de seu tempo'', isto é, desligado de um contexto institucional
historicamente definido, ``isolado na frente'' daqueles com quem está
competindo e aos quais precisa sobrepujar a todo custo. A~fragmentação
dos indivíduos em microorganizações instrumentais assegura um alto grau
de heteronomia interna na universidade, pois o único critério do êxito
competitivo é o controle de todos pelo sistema, expresso em regras
operacionais.\footnote{Cf. a respeito \versal{CHAU}Ì, Marilena. A~Universidade em Ruínas. \emph{in}  \versal{TRINDADE},
H\,(org), \emph{A~Universidade em Ruínas na República dos Professores}.
Petrópolis, Vozes, 1999.} 

Essa descrição do modo pelo qual a universidade vive o seu presente,
relaciona"-se com o passado e prepara"-se para o futuro pode ser resumida
numa palavra: \emph{desfiguração}. No entanto, poder"-se"-ia argumentar,
por que falar em desfiguração justamente no momento em que a
universidade assume a \emph{figura} do seu tempo, os contornos impostos
pelo presente e até mesmo as expectativas geradas pela dimensão material
da cultura? Não são a racionalidade tecnológica e a operacionalidade
características insuperáveis de nossa época e definidoras do sentido que
damos à civilização? A liberdade inscrita nas possibilidades indefinidas
do progresso científico e técnico, não a teríamos conseguido justamente
porque passamos a considerar a racionalidade operacional como um valor?
Será que caberia então questionar o próprio traço que realiza a união
dos termos desse binômio essencial, civilização e cultura? A resposta a
essas perguntas depende da maneira pela qual avaliamos as realizações da
civilização. Com efeito, o olhar empírico e positivista só pode
constatar a vitória do presente, a vitória do homem do presente no mundo
que a ele se apresenta. E~dessa forma se estabelece a identificação, ou
pelo menos a continuidade pacífica, entre \emph{o ser e o dever"-ser}.
Nas palavras de Marcuse:

``Agora podemos expressar o efeito principal desse processo numa
fórmula: a integração do valores culturais na sociedade existente supera
a alienação da cultura frente à civilização, e com isso nivela a tensão
entre o `dever' (\emph{Sollen}) e o ser (\emph{Sein}) (que é uma tensão
real, histórica), entre potencial e atual, futuro e presente, liberdade
e necessidade.''\footnote{\versal{MARCUSE}, H\,Comentários para uma Redefinição de Cultura. \emph{in} 
\emph{Cultura e Sociedade}, vol. 2. São Paulo, Paz e Terra, 1998, p\,160.} 

Normalmente se considera como altamente positivo o fato de ser uma
sociedade ``integrada'', isto é, tornada estável pela composição
coerente dos valores que cultiva. Mas a coerência de um quadro de
valores, e a positividade daí decorrente, pode ter sido estabelecida
pela eliminação das oposições, pela pacificação das tensões e pelo
bloqueio do negativo --- ou seja, por qualquer consideração que não se
restrinja a identificar as potencialidades históricas com o presente
dado --- com o \emph{existente}, para usar um termo frequente em Adorno,
mas que também aparece em Marcuse. A~sociedade pode operar uma
integração de valores num quadro absolutamente estável quando qualquer
tentativa de diferenciação entre \emph{ser e dever"-ser} é imediatamente
remetida à \emph{utopia}. Quanto mais enfraquecida estiver a ação da
negatividade no interior da cultura, tanto mais a civilização se
afirmará como \emph{positiva}. O~triunfo de uma determinada tábua de
valores será tanto maior quanto maior for a capacidade de incorporar ou
eliminar outros valores, dissolvendo assim qualquer oposição. A~vitória
de um conjunto de parâmetros civilizatórios não será completa enquanto
esse conjunto tiver de conviver com outro que lhe seja oposto. Pois a
potência do dever"-ser representa nesse caso sempre algum grau de ameaça
àquilo que é e que se consolidou como \emph{o existente}. É~preciso que
os conteúdos culturais se tornem homogêneos como ``veículos de
adaptação'' para que a civilização do presente possa representar"-se como
triunfante. Dessa maneira a civilização se organiza de modo a
administrar qualquer conflito, erradicar todas as contradições, resolver
qualquer enigma, promover todas as conciliações, de tal maneira que
desapareça a ``tensão real, histórica'' entre a vida administrada e as
potencialidades humanas. É~a partir dessa identificação entre realidade
e necessidade que surge a significação eminentemente ``edificante'' da
cultura, tornada assim uma pedagogia da adaptação. O~esvaziamento das
tensões redunda na plena funcionalidade: tudo aquilo que não confirma
essa funcionalidade é visto como não relacionado com a realidade, e é
decretado como ``irracional'' pela racionalidade tecnológica dominante.
Isso faz com que os valores nascidos de uma experiência ativa de
contestação --- autonomia, liberdade, igualdade --- sejam retraduzidos
de modo a perderem o potencial crítico e negativo inerente à sua gênese.
Assim deve ser para que se reproduza o caráter dominante da
racionalidade tecnológica. E~disso se encarrega a educação.

``A educação para uma independência intelectual e pessoal soa como se
fosse um objetivo geralmente reconhecido. Em realidade, trata"-se aqui de
um programa por demais subversivo, que encerra a violação de alguns dos
mais sólidos tabus democráticos. Pois a cultura democrática dominante
promove a heteronomia sob a máscara da autonomia, impede o
desenvolvimento das necessidades e limita o pensamento e a experiência
sob o pretexto de estendê"-los ao longe por toda parte. \redondo{[…]} A
liberdade mesma opera como veículo de adaptação e
limitação.''\footnote{\versal{MARCUSE}, H\,op.cit., p\,164.} 

A concepção formal dos valores democráticos exige que o potencial das
ideias seja identificado com a realização historicamente possível. E~o
conformismo aparece então como a realização da síntese entre o possível
e o real: desejar mais do que se tem é desestabilizar o adquirido, pondo
em risco a democracia. No entanto as formas possíveis de realização são
determinadas pela racionalidade dominante. Precisamente para se manter
como \emph{dominante}, essa racionalidade tem que limitar o potencial
dos valores enquanto ideias voltadas à transformação. No limite, essa
limitação ocorre como inversão: ``a heteronomia sob a máscara da
autonomia'' e a liberdade operando como ``veículo de adaptação''. Para
que essa inversão tenha sucesso, há que se manter uma experiência
limitada dos valores como instrumentos de transformação. Tal limitação,
por sua vez, só se torna possível pela ausência de uma \emph{crítica da
experiência}. Aquilo que denominamos há pouco desfiguração da
universidade consiste principalmente na sua reorganização de modo a
assegurar de forma sistemática essa ausência.

``Sem essa crítica da experiência o estudante é privado do método e dos
instrumentos intelectuais que o habilitam a compreender sua sociedade e
a cultura desta como um todo na continuidade histórica, na qual se
realiza essa sociedade, que desfigura ou nega suas próprias
possibilidades e promessas. Ao invés disso, o estudante é mais e mais
adestrado para compreender e avaliar relações e possibilidades
estabelecidas somente em \emph{referência às} relações e possibilidades
estabelecidas.''\footnote{\versal{MARCUSE}, H\,op.cit., p\,166.} 

Não é surpreendente que o enaltecimento obsessivo do presente produza um
pensamento circular, como se o passado e o futuro fossem construídos
girando"-se um compasso cuja ponta estaria fixada no presente, o que nos
permitiria medir nossa relação com o passado e com o futuro a partir do
presente como única e soberana referência. Partir do estabelecido para
chegar novamente a ele significa \emph{operar} sobre a realidade sem na
verdade \emph{agir} sobre ela. Como se a experiência fosse um lugar
fechado sem portas nem janelas através das quais pudéssemos vislumbrar
outras possibilidades e outras relações. É~dessa forma que o futuro pode
se sobrepor ao presente, que podemos medir um pelo outro e nos situarmos
indiferentemente num ou noutro ponto de um mesmo lugar. Não admira que o
presente seja dominado pelo pensamento único, se nem mesmo o futuro pode
ser pensado de \emph{outra} maneira. Não admira também que, só podendo
transitar do estabelecido ao estabelecido, o conhecimento seja
valorizado pela sua pretensa neutralidade, como se a grande virtude das
ideias fosse a de passarem desapercebidas, como espectadoras anônimas no
jogo dos interesses humanos.

No entanto, nossa civilização moderna, não somente nos seus aspectos
``espirituais'' mas também naquilo que tem realizado no domínio prático,
deve tudo às ideias (pensamento científico e filosófico) por via das
quais os caminhos históricos foram abertos. E~no alvorecer da
modernidade assistimos o embate entre o pensamento teórico e os
componentes opressores da praxis, que traduziam as ideias consolidadas e
cristalizadas. O~caráter libertador desse conflito está na gênese da
história moderna, e nos habituamos a ver nele um princípio de
emancipação. Mas hoje já podemos por em dúvida o caráter libertador da
ciência, já que o progresso da racionalidade instrumental fez com que os
fins fossem absorvidos pelos meios, a um tal ponto que a racionalidade
passou a corroborar, e não a contestar, a opressão da praxis.

Daí a necessidade de uma ``crítica da experiência'', de um
distanciamento do presente, de uma outra articulação da temporalidade
histórica, para que possamos resgatar, pelo menos pelo pensamento, as
``possibilidades e promessas'' perdidas ao longo da história. Talvez a
esperança de um futuro mais humano dependa em grande parte da capacidade
de ressuscitar promessas mortas prematuramente para incorporá"-las aos
nossos projetos, não para repetir o passado, mas para construir um
futuro que não seja apenas a reiteração do presente. Porque somente uma
ação política e cultural articulada pela memória histórica pode criar
condições para que possamos nos \emph{opor} ao estabelecido e ao que
está em vias de se estabelecer. A~atitude de oposição vai"-se tornando
cada vez mais difícil quanto mais a liberdade vai sendo ajustada aos
padrões de eficiência individual e produtividade organizada. Trata"-se de
um processo de incorporação da subjetividade ao sistema, de forma a que
o exercício da autonomia seja substituído pelo desempenho padronizado: a
produção, transmissão e assimilação de conhecimento tornam"-se questões
de perícia e treinamento.\footnote{``O indivíduo eficiente é aquele cujo desempenho consiste numa ação
somente enquanto reação adequada às demandas objetivas do aparato, e a
liberdade do indivíduo está confinada à seleção dos meios mais adequados
para alcançar uma meta que ele não determinou.'' (\versal{MARCUSE}, H\,Algumas
Implicações Sociais da Tecnologia. \emph{in}  \emph{Tecnologia, Guerra e
Fascismo}. \versal{EDUNESP}, São Paulo, 1999, p\,78.}  Somente uma oposição
crítica a esse império da factualidade pode criar \emph{projetos}, isto
é, visões de futuro que não se subordinem aos fatos, mas que instituam
valores --- aceitando com eles o risco inerente da irrealização, E como
projetos humanos relacionam"-se com a liberdade, talvez ainda possamos
supor que o futuro da universidade, se está sendo gestado no presente,
certamente não será inteiramente determinado por este presente, mas,
seja qual for esse futuro, penso que deve incluir antes de mais nada
nossa opção pela possibilidade de escolhê"-lo, por mais difícil e remota
que nos pareça hoje a possibilidade de realizá"-lo.


